﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendshapeMapper : MonoBehaviour
{
    private SkinnedMeshRenderer this_skinnedMeshRenderer;

    [SerializeField]
    private SkinnedMeshRenderer master_skinnedMeshRenderer;

    public float blendshapeRangeRestrictor = 0.9f;
    public string[] blendshapeNamesToAffect;

    // Start is called before the first frame update
    void Start()
    {
        this_skinnedMeshRenderer = transform.GetComponent<SkinnedMeshRenderer>();
        //master_skinnedMeshRenderer = GameObject.Find("BlendShapeTarget").GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateLocalBlendshapeWeights();
    }

    private void UpdateLocalBlendshapeWeights() {
        if (!this_skinnedMeshRenderer)
            return;

        for (int i = 0; i < this_skinnedMeshRenderer.sharedMesh.blendShapeCount; i++)
        {
            for (int j = 0; j < master_skinnedMeshRenderer.sharedMesh.blendShapeCount; j++)
            {
                var n = master_skinnedMeshRenderer.sharedMesh.GetBlendShapeName(j);
                n = n.Remove(0, 11);
                //Debug.Log("n: " + n);

                if (this_skinnedMeshRenderer.sharedMesh.GetBlendShapeName(i).Contains(n))
                {

                    var index = this_skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex(this_skinnedMeshRenderer.sharedMesh.GetBlendShapeName(i));

                    this_skinnedMeshRenderer.SetBlendShapeWeight(index, master_skinnedMeshRenderer.GetBlendShapeWeight(j));

                    for (int k = 0; k < blendshapeNamesToAffect.Length; k++) {
                        if (this_skinnedMeshRenderer.sharedMesh.GetBlendShapeName(i).ToString() == blendshapeNamesToAffect[k]) {
                            this_skinnedMeshRenderer.SetBlendShapeWeight(index, master_skinnedMeshRenderer.GetBlendShapeWeight(j) * blendshapeRangeRestrictor);
                        }
                    }
                    
                }
            }
        }
    }
}
