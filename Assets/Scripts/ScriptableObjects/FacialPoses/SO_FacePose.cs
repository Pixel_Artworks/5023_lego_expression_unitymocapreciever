﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/FacialPoseEvents/FacePose", order = 1)]
public class SO_FacePose : ScriptableObject
{
    //[SerializeField]
    //public Pose[] poses;
    [SerializeField]
    public List<Pose> poses = new List<Pose>();

    [SerializeField]
    public SOEvent_Bool so_event_bool;

}