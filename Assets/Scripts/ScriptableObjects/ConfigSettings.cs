﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ConfigSettings", order = 1)]
public class ConfigSettings : ScriptableObject
{
    public float transitionSceneDuration = 0f;
    public float mocapSceneTimeout = 0f;
    public float conversionSceneTimeout = 0f; 
    public float userUndetectedTimeout = 0f;
}