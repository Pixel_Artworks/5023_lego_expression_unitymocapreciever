﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SceneRigController : MonoBehaviour
{
    // Start is called before the first frame update
    public float device1_Pos = 256f;
    public float device2_Pos = 320f;

    public float camRot_Offset_1 = -25f;
    public float camRot_Offset_2 = 25f;
    public Transform camOffsetTransform;

    public bool logConsole = false;
    private bool isTrackingFace = false;

    private bool isDevice1 = true;

    private Quaternion rotation;

    private void Start() {
        rotation = new Quaternion(0f, 0f, 0f, 0f);
    }


    public void SwitchCamPosition(bool b) {
        
        StartCoroutine(DOSwitchCamPosition(b));
    }

    private IEnumerator DOSwitchCamPosition(bool b) {
        if (logConsole) {
            Debug.Log("Toggle scene camera height");
        }
        
        if (!b)
        {
            isDevice1 = true;
            transform.DOLocalMoveY(device1_Pos, 0.25f);
        }
        else {
            isDevice1 = false;
            transform.DOLocalMoveY(device2_Pos, 0.25f);  
        }


        //GameObject.FindObjectOfType<FacialTrackingEvents>().CheckIfTracking();
        //isTrackingFace = false;


        StartCoroutine(DOSetApplyOffset());

        yield return new WaitForSeconds(1f);    
    }

    public void SetIsTrackingFace(bool b) {

        isTrackingFace = b;

        StartCoroutine(DOSetApplyOffset());
    }

    private IEnumerator DOSetApplyOffset() {

        var offset = 0f;
        if (isDevice1)
        {
            offset = camRot_Offset_1;
        }
        else {
            offset = camRot_Offset_2;
        }

        if (!isTrackingFace)
        {
            rotation = Quaternion.Euler(0f, offset, 0f);
        }
        else
        {
            rotation = Quaternion.Euler(0f, 0f, 0f);
        }

        camOffsetTransform.DOLocalRotateQuaternion(rotation, 0.5f);

        yield return new WaitForSeconds(0.5f);
    }
}
