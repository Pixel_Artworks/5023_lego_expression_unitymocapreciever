﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class iPhoneMocapController : MonoBehaviour
{
    public CustomStringEvent acceptMessages;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AcceptMessages()
    {
        if (NetworkMeshAnimator.Instance.IsAcceptingMessages())
        {
            NetworkMeshAnimator.Instance.StopAcceptingMessages();
            acceptMessages.Invoke("Start Accepting Messages");
        }
        else
        {
            NetworkMeshAnimator.Instance.StartAcceptingMessages();
            acceptMessages.Invoke("Stop Accepting Messages");
        }
    }
}

