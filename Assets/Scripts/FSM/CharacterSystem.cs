﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using RenderHeads.Media.AVProVideo;

public class CharacterSystem : StateMachine
{
    public Transform CharacterParent;
    public List<GameObject> CharacterPrefabs = new List<GameObject>();
    public GameObject[] characterBackgrounds;

    public float idleStateTimeout = 5f;
    public float transitionStateDuration = 3f;
    public float mocapStateTimeout = 30f;
    public float conversionStateTimeout = 10f;

    public SOEvent_Integer soSetVideoState;
    public SOEvent soSampleUserHeight;

    [HideInInspector]
    public int CharacterPrefabIndex = 0;

    [HideInInspector]
    public int deviceIndex = 0;

    [SerializeField]
    private ExtensionsToggleGroup extensionsToggleGroup;

    private bool m_userDetected = false;

    public bool editMode = false;
    public bool stateLogConsole = false;
    private Coroutine currentCoroutine;

    [SerializeField]
    MediaPlayer mediaPlayer;

    [SerializeField]
    DisplayIMGUI displayIMGUI;

    private string previousState;


    // Start is called before the first frame update
    void Start()
    {
        if (editMode) {
            SetMocapState();
            soSetVideoState.Raise(3);
        }
        else {
            SetIdleState();
        }

        mediaPlayer.Events.AddListener(OnMediaPlayerEvent);

        extensionsToggleGroup.onToggleGroupChanged.AddListener((bool b)=> {
            HandleToggleGroupChange(b);
        });
    }

    private void OnDestroy()
    {
        mediaPlayer.Events.RemoveListener(OnMediaPlayerEvent);
    }

    public void OnMediaPlayerEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.Started:
                displayIMGUI._color = Color.white;
                break;
            case MediaPlayerEvent.EventType.FinishedPlaying:
                displayIMGUI._color = Color.clear;
                mediaPlayer.Rewind(true);
                break;
        }
    }

    public void ToggleDeviceIndex(bool b)
    {
        if (b)
        {
            deviceIndex = 1;
        }
        else
        {
            deviceIndex = 0;
        }
    }

    int r_index = 0;
    int last_r_index = 0;
    public void RandomizeCharacter() {

        while (r_index == last_r_index) {
            r_index = Random.Range(0, CharacterPrefabs.Count); //TODO: Check this
        }
        
        CharacterPrefabIndex = r_index;

        GameObject[] toggles = GameObject.FindGameObjectsWithTag("CharacterToggle");
        toggles[r_index].GetComponent<ExtensionsToggle>().IsOn = true;

        last_r_index = r_index;
        
    }

    private void HandleToggleGroupChange(bool b) {
        //Play transition clip
        PlayCharacterTransitrionClip();
        StartCoroutine(DelayHandleToggleGroupChange(b));
    }

    private IEnumerator DelayHandleToggleGroupChange(bool b) {
        yield return new WaitForSeconds(1f);
        foreach (ExtensionsToggle t in GameObject.Find("Panel_Padding").GetComponentsInChildren<ExtensionsToggle>())
        {
            if (t.IsOn)
            {
                string s = t.gameObject.name.Substring(t.gameObject.name.Length - 1);

                try
                {
                    CharacterPrefabIndex = int.Parse(s);
                }
                catch (System.FormatException e)
                {
                    Debug.Log(e);
                }

                if (CharacterPrefabs.Count >= CharacterPrefabIndex + 1)
                {
                    ExitState();
                    SetMocapState();
                }
            }
        }

        for (int i = 0; i < characterBackgrounds.Length; i++) {

            if (i == CharacterPrefabIndex)
            {
                characterBackgrounds[i].SetActive(true);
            }
            else {
                characterBackgrounds[i].SetActive(false);
            }
            
        }

        try
        {
            FindObjectOfType<FacialTrackingEvents>().CheckIfTracking();
        }
        catch (System.Exception e)
        {
            Debug.Log("FacialTrackingEvents does not exist: " + e);
        }


    }

    public void PlayCharacterTransitrionClip() {
        displayIMGUI._color = Color.white;
        mediaPlayer.Play();
    }

    public void SetIdleState()
    {
        SetState(new IdleState(this, m_userDetected));
    }

    public void SetTransitionState()
    {
        SetState(new TransitionState(this, m_userDetected));
    }

    public void SetMocapState()
    {
        SetState(new MocapState(this, m_userDetected));
    }

    public void SetConversionState()
    {

        SetState(new ConversionState(this, m_userDetected));
    }

    public void Idle() {
        if (currentCoroutine != null) { StopCoroutine(currentCoroutine); }
        currentCoroutine = StartCoroutine(State.Idle());
        previousState = State.ToString();
    }

    public void Transition()
    {
        if (currentCoroutine != null) { StopCoroutine(currentCoroutine); }
        currentCoroutine = StartCoroutine(State.Transition());
        previousState = State.ToString();
    }

    public void Mocap()
    {
        if (currentCoroutine != null) { StopCoroutine(currentCoroutine); }
        currentCoroutine = StartCoroutine(State.Mocap());

        if (previousState == "TransitionState")
        {
            StartCoroutine(State.Timeout());
            previousState = State.ToString();
        }
    }

    public void Conversion()
    {
        if (currentCoroutine != null) { StopCoroutine(currentCoroutine); }
        currentCoroutine = StartCoroutine(State.Conversion());
        previousState = State.ToString();
    }

    public void ExitState()
    {
        if (currentCoroutine != null) { StopCoroutine(currentCoroutine); }
        currentCoroutine = StartCoroutine(State.ExitState());
        previousState = State.ToString();
    }

    public void SetDevice(int i) {
        deviceIndex = i;
    }

    public void OnUserDetection(bool b) {

        if (editMode)
            return;

        m_userDetected = b;

        if (stateLogConsole) {
            Debug.Log("m_userDetected: " + m_userDetected);
        }

        ExitState();
        SetTransitionState();
    }
    
}

