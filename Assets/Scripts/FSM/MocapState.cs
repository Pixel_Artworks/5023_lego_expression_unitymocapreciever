﻿using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class MocapState : State
{
    private GameObject character;

    public MocapState(CharacterSystem characterSystem, bool userDetected) : base(characterSystem, userDetected)
    {
    }
    // Start is called before the first frame update
    public override IEnumerator Start()
    {
        if (CharacterSystem.stateLogConsole)
        {
            UnityEngine.Debug.Log("Mocap");
        }

        character = GameObject.Instantiate(CharacterSystem.CharacterPrefabs[CharacterSystem.CharacterPrefabIndex], CharacterSystem.CharacterParent);
        character.transform.SetParent(CharacterSystem.CharacterParent);

        NetworkMeshAnimator.Instance.StartAcceptingMessages();

        CharacterSystem.Mocap();
        yield break;
    }

    public override IEnumerator Mocap()
    {
        if (CharacterSystem.editMode)
            yield break;

        if (!UserDetected)
        {
            CharacterSystem.SetIdleState();
        }

        CharacterSystem.soSetVideoState.Raise(3);

        /*
        UnityEngine.Debug.Log("Mocap State starting tiomeout countdown from: " + CharacterSystem.mocapStateTimeout + " seconds");
        yield return new WaitForSeconds(CharacterSystem.mocapStateTimeout);
        UnityEngine.Debug.Log("Mocap State timed out!");
        CharacterSystem.ExitState();
        CharacterSystem.SetConversionState();
        */

        yield break;
    }

    public override IEnumerator Timeout() {

        if (CharacterSystem.stateLogConsole) {
            UnityEngine.Debug.Log("Mocap State starting timeout countdown from: " + CharacterSystem.mocapStateTimeout + " seconds");
        }

        yield return new WaitForSeconds(CharacterSystem.mocapStateTimeout);

        if (CharacterSystem.stateLogConsole) {
            UnityEngine.Debug.Log("Mocap State timed out!");
        }

        CharacterSystem.ExitState();
        CharacterSystem.SetConversionState();

        yield break;
    }

    public override IEnumerator ExitState() {
        //UnityEngine.Debug.Log("EXIT MocapState");
        NetworkMeshAnimator.Instance.StopAcceptingMessages();
        GameObject.Destroy(character);
        yield break;
    }
}
