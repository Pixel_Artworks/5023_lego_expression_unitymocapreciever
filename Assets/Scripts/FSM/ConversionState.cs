﻿using System.Collections;
using System.Diagnostics;
using UnityEngine;

public class ConversionState : State
{

    public ConversionState(CharacterSystem characterSystem, bool userDetected) : base(characterSystem, userDetected)
    {
    }
    // Start is called before the first frame update
    public override IEnumerator Start()
    {
        if (CharacterSystem.stateLogConsole)
        {
            UnityEngine.Debug.Log("Conversion");
        }
        CharacterSystem.Conversion();
        yield break;
    }

    public override IEnumerator Conversion()
    {
        CharacterSystem.soSetVideoState.Raise(2);

        yield return new WaitForSeconds(CharacterSystem.conversionStateTimeout);
        CharacterSystem.ExitState();
        CharacterSystem.SetIdleState();
        yield break;
    }

    public override IEnumerator ExitState() {
        //UnityEngine.Debug.Log("EXIT ConversionState");
        yield break;
    }

}
