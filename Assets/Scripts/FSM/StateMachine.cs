﻿using System;
using UnityEngine;

public abstract class  StateMachine : MonoBehaviour
{
    protected State State;

    public void SetState(State state) {
        if (State != null) {
            State.ExitState();
        }
        


        State = state;
        StartCoroutine(State.Start());
    }
}
