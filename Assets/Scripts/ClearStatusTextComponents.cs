﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearStatusTextComponents : MonoBehaviour
{

    public UnityEngine.UI.Text[] statusTexts;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClearStatus(bool b) {
        if (b)
            return;

        foreach (UnityEngine.UI.Text t in statusTexts) {
            t.text = string.Format("...");
        }


    }
}
