﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyboardInput : MonoBehaviour
{

    public CustomBoolEvent i_Key_Event;
    private bool i_KeyToggle = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("q"))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown("i"))
        {
            i_KeyToggle = !i_KeyToggle;
            i_Key_Event.Invoke(i_KeyToggle);
        }
    }
}

[System.Serializable]
public class CustomBoolEvent : UnityEvent<bool>
{
}

[System.Serializable]
public class CustomStringEvent : UnityEvent<string>
{
}
