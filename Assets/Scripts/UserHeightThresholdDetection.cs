﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using com.rfilkov.components;
using System;


[RequireComponent(typeof(BodySlicer))]
public class UserHeightThresholdDetection : MonoBehaviour
{
    private BodySlicer bodySlicer;
    private HeightEstimator heightEstimator;

    private float estimatedHeight;
    public float maxHeightThreshhold = 1.2f;
    public UnityEngine.UI.Text statusText;

    public bool bypassHeightCheck = false;

    public bool logConsole = false;

    private bool isUserHeightAboveThreshold;

    public bool IsUserHeightAboveThreshold
    {
        get { return isUserHeightAboveThreshold; }
        set
        {
            if (value == isUserHeightAboveThreshold)
                return;

            isUserHeightAboveThreshold = value;
            if (isUserHeightAboveThreshold)
            {
                if (logConsole) {
                    Debug.Log("User is above Threshold");
                }
                
                
            }
            else
            {
                if (logConsole) {
                    Debug.Log("User is below Threshold");
                }
                
            }

            //so_Bool_FacialTrackDeviceSelection.Raise(!isUserHeightAboveThreshold);
        }
    }

    [SerializeField]
    private SOEvent_Bool so_Bool_FacialTrackDeviceSelection;


    void Start()
    {
        bodySlicer = transform.GetComponent<BodySlicer>();
        bodySlicer.enabled = false;
        bodySlicer.enabled = true;

        heightEstimator = transform.GetComponent<HeightEstimator>();
    }

    // Update is called once per frame
    void Update()
    {
        //CheckHeight();
    }

    public void CheckHeight(bool b)
    {
        if (bypassHeightCheck)
        {
            if (logConsole) {
                Debug.Log("Bypassing height check -- using default child position");
            }
            so_Bool_FacialTrackDeviceSelection.Raise(true);
        }
        else {
            if (!b)
                return;
            StartCoroutine(SampleHeight());
        }
        
    }

    IEnumerator SampleHeight() {
        float duration = Time.time + 1.0f;
        while (Time.time < duration)
        {
            if (logConsole) {
                Debug.Log("Sampling height...");
            }
            
            statusText.text = string.Format("Sampling height..." + estimatedHeight);
            //estimatedHeight = bodySlicer.getUserHeight();
            estimatedHeight = heightEstimator.userHeight;
            yield return null;
        }

        //estimatedHeight = bodySlicer.getUserHeight();
        estimatedHeight = heightEstimator.userHeight;
        if (logConsole) {
            Debug.Log("Final Estimated Height: " + estimatedHeight);
        }
        
        statusText.text = string.Format("Final Estimated Height: " + estimatedHeight);

        if (estimatedHeight < maxHeightThreshhold)
        {
            //IsUserHeightAboveThreshold = true;
            so_Bool_FacialTrackDeviceSelection.Raise(true);
        }
        else
        {
            //IsUserHeightAboveThreshold = false;
            so_Bool_FacialTrackDeviceSelection.Raise(false);
        }
        yield break;

    }
}
