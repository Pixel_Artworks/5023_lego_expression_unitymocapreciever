﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using com.rfilkov.components;

public class UserDetectionLogic : MonoBehaviour
{
    private float sceneLoadLeadTime = 0f;
    private float mocapTimeout = 0f;
    private float conversionTimeout = 0f;

    private bool isUserDetected = false;

    [SerializeField]
    private ConfigSettingsContainer configSettingsContainer;

    private void Start()
    {
        

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void Update()
    {

    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded - Scene: " + scene.name);
        mocapTimeout = configSettingsContainer.configSettings.mocapSceneTimeout;
        conversionTimeout = configSettingsContainer.configSettings.conversionSceneTimeout;

        //UserDetectionSceneSelection(FindObjectOfType<SkeletonCollider>().isInRange);


        if (SceneLoader.GetCurrentScene() == SceneLoader.Scene.MocapScene.ToString())
        {
            Debug.Log("loaded mocap scene -- timing out in " + mocapTimeout + " seconds");
            StartCoroutine(LoadSceneWithDelay(mocapTimeout, SceneLoader.Scene.ConversionScene));
        }

        if (SceneLoader.GetCurrentScene() == SceneLoader.Scene.ConversionScene.ToString())
        {
            Debug.Log("loaded conversion scene -- moving back to idle");
            StartCoroutine(LoadSceneWithDelay(conversionTimeout, SceneLoader.Scene.IdleScene));
        }
    }

    //TODO: Need a timer function in the skeleton visualiser that says how long has been detected for then resets when lost that checks if user is still in scene for XX seconds prior to opening scene
    public void UserDetectionSceneSelection(bool b) {
        isUserDetected = b;
        //Debug.Log("User detected and is in range: " + b);
        //Debug.Log("Current Scene is: " + SceneLoader.GetCurrentScene());

        if (SceneLoader.GetCurrentScene() == SceneLoader.Scene.IdleScene.ToString() && !isUserDetected)
        {
            //Debug.Log("The current scene is: " + SceneLoader.GetCurrentScene() + " and a user is not in range");

        } else if (SceneLoader.GetCurrentScene() == SceneLoader.Scene.IdleScene.ToString() && isUserDetected)
        {
            //Debug.Log("The current scene is: " + SceneLoader.GetCurrentScene() + " and a user is in range -- open MocapScene");
            StartCoroutine(LoadSceneWithDelay(sceneLoadLeadTime, SceneLoader.Scene.MocapScene));

        } 
        
        else if (SceneLoader.GetCurrentScene() == SceneLoader.Scene.MocapScene.ToString() && !isUserDetected)
        { 
            //Debug.Log("The current scene is: " + SceneLoader.GetCurrentScene() + " and a user is in range -- open IdleScene");
            StartCoroutine(LoadSceneWithDelay(0f, SceneLoader.Scene.IdleScene));

        }
        
        /*
        
        else if (SceneLoader.GetCurrentScene() == SceneLoader.Scene.MocapScene.ToString() && isUserDetected)
        {
            //Debug.Log("The current scene is: " + SceneLoader.GetCurrentScene() + " and a user is in range -- open IdleScene");
            StartCoroutine(LoadSceneWithDelay(2f, SceneLoader.Scene.ConversionScene));
        }
        */
    }


    private IEnumerator LoadSceneWithDelay(float f, SceneLoader.Scene s) {
        yield return new WaitForSeconds(f);
        SceneLoader.LoadScene(s, "");
    }

}
