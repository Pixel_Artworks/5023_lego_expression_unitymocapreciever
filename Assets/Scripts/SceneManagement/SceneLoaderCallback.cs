﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SceneLoaderCallback : MonoBehaviour
{

    private ConfigSettings configSettings;
    private bool isFirstUpdate = true;
    private float transitionTime = 0f;

    private void Update() {
        if (isFirstUpdate) {
            isFirstUpdate = false;
            //SceneLoader.SceneLoaderCallback();

            configSettings = FindObjectOfType<ConfigSettingsContainer>().configSettings;
            transitionTime = configSettings.transitionSceneDuration;

            StartCoroutine(CallbackDelay(transitionTime));
        }
    }

    private IEnumerator CallbackDelay(float t) {
        yield return new WaitForSeconds(t);
        SceneLoader.SceneLoaderCallback();
    }
}


