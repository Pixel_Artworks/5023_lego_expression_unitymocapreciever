﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AppLoader : MonoBehaviour
{
    //UI
    public TMP_InputField transitionDurationInput;
    public TMP_InputField mocapTimeoutInput;
    public TMP_InputField conversionTimeoutInput;
    public TMP_InputField userLostTimeoutInput;
    public Button launchAppBtn;
    
    //Data
    public ConfigSettings configSettings;

    private ConfigSettingsContainer configSettingsContainer;

    private int transitionDuration = 0;
    private int mocapTimeout = 0;
    private int conversionTimeout = 0;
    private int userLostTimeout = 0;


    void Start()
    {
        transitionDurationInput.text = configSettings.transitionSceneDuration.ToString();
        mocapTimeoutInput.text = configSettings.mocapSceneTimeout.ToString();
        conversionTimeoutInput.text = configSettings.conversionSceneTimeout.ToString();
        userLostTimeoutInput.text = configSettings.userUndetectedTimeout.ToString();


        configSettingsContainer = FindObjectOfType<ConfigSettingsContainer>();
        configSettingsContainer.configSettings = configSettings;

        transitionDurationInput.onValueChanged.AddListener((string s)=> {
            transitionDuration = int.Parse(s);
            configSettings.transitionSceneDuration = transitionDuration;
        });

        mocapTimeoutInput.onValueChanged.AddListener((string s) => {
            mocapTimeout = int.Parse(s);
            configSettings.mocapSceneTimeout = mocapTimeout;
        });

        conversionTimeoutInput.onValueChanged.AddListener((string s) => {
            conversionTimeout = int.Parse(s);
            configSettings.conversionSceneTimeout = transitionDuration;
        });

        userLostTimeoutInput.onValueChanged.AddListener((string s) => {
            userLostTimeout = int.Parse(s);
            configSettings.userUndetectedTimeout = userLostTimeout;
        });

        launchAppBtn.onClick.AddListener(()=> {
            SceneLoader.LoadScene(SceneLoader.Scene.IdleScene, "");
        });
    }
}
