﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacialTrackingEvents : MonoBehaviour
{

    private bool m_isTracked = false;
    public bool IsTracked
     {
        get { return m_isTracked; }
        set
        {
            if (m_isTracked != value) {
                m_isTracked = value;

                FacialTrackingEvents_IsTrackedChanged(m_isTracked);
                //Debug.Log("Is Tracking face: " + m_isTracked);
            }
        }
    }

    [SerializeField]
    private SOEvent_Bool so_faceIsTracked;

    // Start is called before the first frame update
    void Start()
    {

        IsTracked = NetworkMeshAnimator.isTracking;
    }

    private void FacialTrackingEvents_IsTrackedChanged(bool b)
    {
        //Debug.Log("m_isTracked: " + b);
        so_faceIsTracked.Raise(!b);

    }

    // Update is called once per frame
    void Update()
    {
        if (m_isTracked != NetworkMeshAnimator.isTracking) {
            IsTracked = NetworkMeshAnimator.isTracking;
        }
    }

    public void CheckIfTracking() {
        //Debug.Log("Check if tracking!");
        IsTracked = NetworkMeshAnimator.isTracking;
        so_faceIsTracked.Raise(!IsTracked);
    }
}
