﻿using System;
using System.Linq;
using System.Collections;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Filters;
using DG.Tweening;
using System.Diagnostics;

public class NetworkMeshAnimator {

	private UDPServer listner;
	private SkinnedMeshRenderer meshTarget;
	private Mesh skinnedMesh;
    private int blendShapeCount;
	private UnityMainThreadDispatcher dispatcher;
	private bool isAcceptingMessages = false;
	private Dictionary<string, float> currentBlendShapes;

	private int deviceIndex = 0;
	private string devicePrefix = "id0_";

	public static bool isTracking = false;

	public bool consoleLogMessages = false;


	private static NetworkMeshAnimator instance;

	public static NetworkMeshAnimator Instance
	{
		get 
		{
			if (instance == null)
			{
				instance = new NetworkMeshAnimator();
			}
			return instance;
		}
	}

	private NetworkMeshAnimator() {
		
		this.listner  = new UDPServer ((String message) => { 
			if (isAcceptingMessages) {
				dispatcher.Enqueue (SetBlendShapesOnMainThread (message));
			}
		});


		EditorApplication.playModeStateChanged += PlayStateChanged;

		listner.Start ();

	}
		
	private static void PlayStateChanged(PlayModeStateChange state)
	{
		if (state.Equals (PlayModeStateChange.ExitingPlayMode)) {
		
			instance.StopAcceptingMessages ();
		}
	}

	public void StartAcceptingMessages() {
		if (consoleLogMessages) {
			UnityEngine.Debug.Log("Started accepting messages");
		}
		

		meshTarget = GameObject.Find ("BlendShapeTarget").GetComponent<SkinnedMeshRenderer> ();
		

		if (meshTarget == null) {
			if (consoleLogMessages)
			{
				UnityEngine.Debug.LogError("Cannot find BlendShapeTarget. Have you added it to your scene?");

			}
			return;
		}

		skinnedMesh = meshTarget.GetComponent<SkinnedMeshRenderer>().sharedMesh;
		blendShapeCount = skinnedMesh.blendShapeCount;
		currentBlendShapes = new Dictionary<string, float>();

		if (UnityMainThreadDispatcher.Exists()) {
			dispatcher = UnityMainThreadDispatcher.Instance ();
		} else {
			if (consoleLogMessages)
			{
				UnityEngine.Debug.LogError("Cannot reach BlendShapeTarget. Have you added the UnityMainThreadDispatcher to your scene?");
			}
			
		}

		isAcceptingMessages = true;

	}

	public void StopAcceptingMessages() {
		if (consoleLogMessages)
		{
			UnityEngine.Debug.Log("Stopped accepting messages");
		}
		isAcceptingMessages = false;
	}

	public bool IsAcceptingMessages() {
		return isAcceptingMessages;
	}

	public IEnumerator SetBlendShapesOnMainThread(string messageString) {

		deviceIndex = GameObject.FindObjectOfType<CharacterSystem>().deviceIndex;

		if (deviceIndex == 0)
		{
			devicePrefix = "id0_";
		}
		else {
			devicePrefix = "id1_";
		}

		
		if (messageString.Contains(devicePrefix + "tracking"))
		{
			var m = messageString.Substring(12);
			int p = 0;
			Int32.TryParse(m, out p);
			isTracking = p != 0;

			//UnityEngine.Debug.Log("isTracking: " + isTracking);
		}
		


			if (messageString.Contains(devicePrefix + "position"))
		{
            var m = messageString.Substring(12);

			float tx = 0.0f;
			float ty = 0.0f;
			float tz = 0.0f;
			
			foreach (string message in m.Split(new Char[] { ',' }))
			{
				var cleanString = message.Replace(" ", "").Replace("(", "").Replace(")", "");
				if (cleanString.Contains("x"))
				{
					cleanString = cleanString.Substring(2);
					tx = float.Parse(cleanString) * 100;
				}
				else if (cleanString.Contains("y")) {
					cleanString = cleanString.Substring(3);
					ty = float.Parse(cleanString) * 100;
				}
				else if (cleanString.Contains("z"))
				{
					cleanString = cleanString.Substring(3);
					tz = float.Parse(cleanString) * 100;
				}
			}
			Vector3 position = new Vector3(-tx, ty, -tz);
            if (meshTarget != null) {
                meshTarget.transform.localPosition = position;
            }
			
		}
		else if (messageString.Contains(devicePrefix + "orientation"))
		{
			var m = messageString.Substring(15);


			float rx = 0.0f;
			float ry = 0.0f;
			float rz = 0.0f;
			float rw = 0.0f;

			foreach (string message in m.Split(new Char[] { ',' }))
			{
				var cleanString = message.Replace(" ", "").Replace("(", "").Replace(")", "");
				if (cleanString.Contains("x"))
				{
					cleanString = cleanString.Substring(2);
					rx = float.Parse(cleanString);
				}
				else if (cleanString.Contains("y"))
				{
					cleanString = cleanString.Substring(2);
					ry = float.Parse(cleanString);
				}
				else if (cleanString.Contains("z"))
				{
					cleanString = cleanString.Substring(2);
					rz = float.Parse(cleanString);
				}

                else if (cleanString.Contains("w"))
				{
					cleanString = cleanString.Substring(2);
					rw = float.Parse(cleanString);
				}
                
			}
			Quaternion rotation = new Quaternion(rx, -ry, -rz, rw);

			/*
			Vector3 rotEuler = rotation.eulerAngles;
			
			UnityEngine.Debug.Log("rotation: " + rotEuler);

			var angle = Mathf.Clamp(rotEuler.y, 90, 270);

			UnityEngine.Debug.Log("angle: " + angle);
			*/



			try
			{
				if (meshTarget != null)
				{
					meshTarget.transform.rotation = rotation;
				}
			}
			catch (Exception e)
			{
				UnityEngine.Debug.LogException(e);
			}
			
		}

		else if(messageString.Contains(devicePrefix + "blendshapes"))
		{
			var ms = messageString.Substring(15);
            //UnityEngine.Debug.Log("ms: " + ms);


			foreach (string message in ms.Split(new Char[] { '|' }))
			{

				

				var cleanString = message.Replace(" ", "").Replace("msg:", "");

				
				var strArray = cleanString.Split(new Char[] { '-' });


				if (strArray.Length == 2)
				{
					var weight = float.Parse(strArray.GetValue(1).ToString());

					var mappedShapeName = "blendShape1." + strArray.GetValue(0).ToString().Replace("_L", "Left").Replace("_R", "Right");

                    if (meshTarget == null) {
						try
						{
							meshTarget = GameObject.Find("BlendShapeTarget").GetComponent<SkinnedMeshRenderer>();
						}
						catch (Exception e)
						{
							UnityEngine.Debug.LogException(e);
						}	
                    }

					if (meshTarget != null)
					{
						var index = meshTarget.sharedMesh.GetBlendShapeIndex(mappedShapeName);
					

						if (index > -1)
						{

							float a = meshTarget.GetBlendShapeWeight(index);
							float smoothTime = 0.0275f;
							float yVelocity = 0.0f;
							float b = Mathf.SmoothDamp(a, weight, ref yVelocity, smoothTime);

							meshTarget.SetBlendShapeWeight(index, weight);
							AddOrUpdateDictionaryEntry(mappedShapeName, weight);

						}
					}
				}
			}
		}

		yield return null;
	}

	private void AddOrUpdateDictionaryEntry(string key, float value)
	{
		if (currentBlendShapes.ContainsKey(key))
		{
			currentBlendShapes[key] = value;
		}
		else
		{
			currentBlendShapes.Add(key, value);
		}
	}

	public float BlendShapeValue(string key) {
		try {
			if (currentBlendShapes.ContainsKey(key))
			{
				float val = currentBlendShapes[key];
				return val;
			}
			else {
				return float.NaN;
			}
		}catch(Exception e)
        {
			UnityEngine.Debug.Log(e);
			return float.NaN;
		}

    }
}



