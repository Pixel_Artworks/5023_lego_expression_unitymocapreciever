﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class VideoPlaybackCanvasVisibility : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup cg;
    private int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        cg.alpha = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetVisibilityFromStateIndex(int i) {
        index = i;
        if (index == 3)
        {
            cg.alpha = 0f;
        }
        else {
            cg.alpha = 1f;
        }
    }

    public void FadeInClip(float t) {
        StartCoroutine(DOFadeInClip(t));
    }

    private IEnumerator DOFadeInClip(float t) {
        cg.DOFade(1f, t);
        yield return new WaitForSeconds(t);
    }

    public void FadeOutClip(float t)
    {
        if (index > 2)
        {
            StartCoroutine(DOFadeOutClip(t));
        }
    }

    private IEnumerator DOFadeOutClip(float t)
    {
        cg.DOFade(0f, t);
        yield return new WaitForSeconds(t);
    }
}
