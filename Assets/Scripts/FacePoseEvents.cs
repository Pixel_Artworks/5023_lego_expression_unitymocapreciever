﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FacePoseEvents : MonoBehaviour
{
    
    public SO_FacePose so_poses;

    private SkinnedMeshRenderer skinnedMeshRenderer;
    int blendShapeCount = 0;
    private int poseCount = 0;
    private bool[] poseBools;

    private bool m_IsAllTrue = false;
    private bool prev_m_IsAllTrue = false;

    public float rangeWidth = 50f;

    public bool useTongueOut = true;
    // Start is called before the first frame update
    void Start()
    {
        skinnedMeshRenderer = transform.GetComponent<SkinnedMeshRenderer>();
        blendShapeCount = skinnedMeshRenderer.sharedMesh.blendShapeCount;

        poseCount = so_poses.poses.Count;
        poseBools = new bool[poseCount];
        for (int i = 0; i < poseBools.Length; i++) {
            poseBools[i] = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        CompareBlendShapeValues();

        m_IsAllTrue = IsAllTrue(poseBools);

        if (m_IsAllTrue != prev_m_IsAllTrue) {
            //Debug.Log("RAISING FACE POSE EVENT!");
            if (m_IsAllTrue) {
                so_poses.so_event_bool.Raise(m_IsAllTrue);
            }

            
            prev_m_IsAllTrue = m_IsAllTrue;
        }

        
    }

    private void CompareBlendShapeValues() {

        if (!useTongueOut)
        {
            if (!so_poses)
                return;

            if (so_poses.poses[0] == null)
                return;

            for (int i = 0; i < poseCount; i++)
            {

                for (int j = 0; j < blendShapeCount; j++)
                {
                    if (skinnedMeshRenderer.sharedMesh.GetBlendShapeName(j).Contains(so_poses.poses[i].name))
                    {
                        var current = skinnedMeshRenderer.GetBlendShapeWeight(j);
                        var min = so_poses.poses[i].valueMin;
                        var max = so_poses.poses[i].valueMax;

                        poseBools[i] = IsBetween(current, min, max);
                        //Debug.Log("Blendshape " + skinnedMeshRenderer.sharedMesh.GetBlendShapeName(j) + " is in range?: " + b);
                    }
                }
            }
        }
        else {
            var index = skinnedMeshRenderer.sharedMesh.GetBlendShapeIndex("blendShape1.tongueOut");
            if (skinnedMeshRenderer.GetBlendShapeWeight(index) > 50) {
                m_IsAllTrue = true;
                so_poses.so_event_bool.Raise(m_IsAllTrue);
            }

        }
        
    }

    private bool IsBetween(double input, double bound1, double bound2)
    {
        if (bound1 > bound2)
            return input >= bound2 && input <= bound1;
        return input >= bound1 && input <= bound2;
    }

    private bool IsAllTrue(bool[] input)
    {
        for (int i = 0; i < input.Length; ++i)
        {
            if (input[i] == false)
            {
                return false;
            }
        }

        return true;
    }

    public void SavePoseState() {
        //Debug.Log("SavePoseState");
        blendShapeCount = skinnedMeshRenderer.sharedMesh.blendShapeCount;
        so_poses.poses.Clear();
        for (int i = 0; i < blendShapeCount; i++) {
            Pose p = new Pose();
            so_poses.poses.Add(p);
            so_poses.poses[i].name = skinnedMeshRenderer.sharedMesh.GetBlendShapeName(i);
            so_poses.poses[i].valueMin = skinnedMeshRenderer.GetBlendShapeWeight(i) - rangeWidth;
            so_poses.poses[i].valueMax = skinnedMeshRenderer.GetBlendShapeWeight(i) + rangeWidth;
        }

        saveData();


    }

    private void saveData()
    {
        
        EditorUtility.SetDirty(so_poses);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        Debug.Log("Saved Blendshape Pose Data");
    }
}
