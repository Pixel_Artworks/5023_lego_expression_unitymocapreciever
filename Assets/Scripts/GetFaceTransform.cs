﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Filters;

public class GetFaceTransform : MonoBehaviour
{
    [SerializeField]
    public Transform face_transform;

    [SerializeField]
    public Transform ragdoll_transform;

    
    public Vector3 minRotations = new Vector3(0f, 0f, 0f);
    public Vector3 maxRotations = new Vector3(0f, 0f, 0f);
    

    public Vector4 rotationMultiplier = new Vector4(1f, 1f, 1f, 1f);
    public float new_Q = 0.000001f;
    public  float new_R = 0.01f;

    public float tween = 0.25f;

    public bool debug = false;

    public bool logConsole = false;

    private Transform sceneRig;

    //private KalmanFilterVector3 k1, k2, k3;

    void OnEnable()
    {
        if (logConsole) {
            Debug.Log("FaceTransform Enabled");
        }
        
        sceneRig = GameObject.FindGameObjectWithTag("SceneRig").GetComponent<Transform>() ;

        //k1 = new KalmanFilterVector3();
        //k2 = new KalmanFilterVector3();
        //k3 = new KalmanFilterVector3();
    }


    private void LateUpdate()
    {
        if (sceneRig != null && face_transform != null) {

            if (!debug)
            {
                Quaternion faceRotation = new Quaternion(face_transform.rotation.x * rotationMultiplier.x, face_transform.rotation.y * rotationMultiplier.y, face_transform.rotation.z * rotationMultiplier.z, face_transform.rotation.w * rotationMultiplier.w);

                Vector3 rot = faceRotation.eulerAngles;
                Vector3 clampedRot = new Vector3(rot.x, 0f, rot.z);
                Quaternion newFaceRotation = Quaternion.Euler(clampedRot);

                ragdoll_transform.DORotateQuaternion(newFaceRotation, tween);
            }

            //sceneRig.DORotateQuaternion(Quaternion.Inverse(new Quaternion(sceneRig.rotation.x, -face_transform.rotation.y * rotationMultiplier.y, sceneRig.rotation.z, sceneRig.rotation.w)), 2f);

            Vector3 camRot = Quaternion.Inverse(new Quaternion(face_transform.rotation.x * rotationMultiplier.x, face_transform.rotation.y * rotationMultiplier.y, sceneRig.rotation.z, sceneRig.rotation.w)).eulerAngles;

            float clampedY = Mathf.Clamp(camRot.y, 0, 360);

            Vector3 camClampedRot = new Vector3(camRot.x, clampedY, camRot.z);
            Quaternion newCamRotation = Quaternion.Euler(camClampedRot);

            sceneRig.DORotateQuaternion(newCamRotation, 0.75f);


            //sceneRig.DORotateQuaternion(Quaternion.Inverse(new Quaternion(face_transform.rotation.x * rotationMultiplier.x, face_transform.rotation.y * rotationMultiplier.y, sceneRig.rotation.z, sceneRig.rotation.w)), 0.75f);


        }
        

    }
}
