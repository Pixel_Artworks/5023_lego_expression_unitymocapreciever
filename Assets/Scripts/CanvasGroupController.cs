﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CanvasGroupController : MonoBehaviour
{
    public float fadeDuration = 2f;

    private CanvasGroup cg;
    public bool startOn = false;
    // Start is called before the first frame update
    void Start()
    {
        cg = transform.GetComponent<CanvasGroup>();
        SetVisibility(startOn);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetVisibility(bool b) {
        StartCoroutine(DOSetVisibiilty(b));
    }

    IEnumerator DOSetVisibiilty(bool b) {
        if (b)
        {
            cg.DOFade(1f, fadeDuration);
            cg.interactable = true;
            cg.blocksRaycasts = true;
        }
        else {
            cg.interactable = false;
            cg.blocksRaycasts = false;
            cg.DOFade(0f, fadeDuration);
        }
        yield return new WaitForSeconds(fadeDuration);
    }
}
