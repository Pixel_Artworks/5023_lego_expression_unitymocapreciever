﻿using UnityEngine;
using System.Collections;
using com.rfilkov.kinect;
using System.Collections.Generic;
using DG.Tweening;

namespace com.rfilkov.components
{
    public class PlayerDetection : MonoBehaviour
    {
        [Tooltip("Index of the player, tracked by this component. 0 means the 1st player, 1 - the 2nd one, 2 - the 3rd one, etc.")]
        public int playerIndex = 0;
        [Tooltip("Depth sensor index used for color frame overlay - 0 is the 1st one, 1 - the 2nd one, etc.")]
        public int sensorIndex = 0;
        [Tooltip("Delay in seconds when the replay starts, after no more users have been detected.")]
        public float userLostMaxTime = 5f;
        [Tooltip("Show collision visualisation.")]
        public bool showDebug = false;
        public UnityEngine.UI.Text statusText;

        [SerializeField]
        private Transform playerPositionParent;
        [SerializeField]
        private GameObject playerPositionCollider;
        [SerializeField]
        private GameObject detectionZone;
        private GameObject m_playerPositionCollider;
        [SerializeField]
        private SOEvent_Bool so_userDetectionEvent;

        private KinectManager kinectManager = null;
        private Vector3 playerPosition;
        private float userDetectionConfidence = 0.0f;
        private float lastUserTime = 0f;
        private bool userDetected = false;
        private bool userIsInRange = false;
        private bool isUserDetected = false;

        public bool IsUserDetected
        {
            get { return isUserDetected; }
            set
            {
                if (value == isUserDetected)
                    return;

                isUserDetected = value;
                if (isUserDetected)
                {
                    if (showDebug)
                    {
                        Debug.Log("User Found");
                    }

                    so_userDetectionEvent.Raise(true);
                }
                else
                {
                    if (showDebug)
                    {
                        Debug.Log("User Lost");
                    }
                    so_userDetectionEvent.Raise(false);
                }
            }
        }


        void Start()
        {
            kinectManager = KinectManager.Instance;
            playerPosition = new Vector3(0f, 0f, 0f);

            if (kinectManager && kinectManager.IsInitialized())
            {
                m_playerPositionCollider = Instantiate(playerPositionCollider) as GameObject;
                m_playerPositionCollider.transform.parent = transform;
                m_playerPositionCollider.SetActive(false);

                detectionZone.GetComponent<Renderer>().enabled = userIsInRange;
            }
        }

        void Update()
        {
            if (kinectManager && kinectManager.IsInitialized())
            {
                detectionZone.GetComponent<Renderer>().enabled = showDebug;
                m_playerPositionCollider.GetComponent<Renderer>().enabled = showDebug;

                if (kinectManager.IsUserDetected(playerIndex))
                {
                    ulong userId = kinectManager.GetUserIdByIndex(playerIndex);
                    playerPosition = kinectManager.GetUserPosition(userId);

                    if (showDebug) {
                        Debug.Log("playerPosition: " + playerPosition);
                    }
                    
                    Vector3 modifiedPosition = new Vector3(playerPosition.x, 1f, playerPosition.z);
                    m_playerPositionCollider.transform.position = modifiedPosition;

                    StartCoroutine(DetectionConfidenceSmoothing(1f));
                    userDetected = true;
                }
                else {
                    StartCoroutine(DetectionConfidenceSmoothing(0f));

                }

                if (showDebug) {
                    Debug.Log("userDetectionConfidence: " + userDetectionConfidence);
                }

                if (userDetectionConfidence > 0.01f) {
                    m_playerPositionCollider.SetActive(true);
                    
                }
                else {
                    m_playerPositionCollider.SetActive(false);

                    userDetected = false;
                    userIsInRange = false;
                }

                if (userDetected) {
                    float dist = Vector3.Distance(m_playerPositionCollider.transform.position, detectionZone.transform.position);

                    if (dist < (detectionZone.transform.localScale.x + detectionZone.transform.localScale.z) / 2)
                    {
                        userIsInRange = true;
                        detectionZone.GetComponent<Renderer>().material.color = new Color(detectionZone.GetComponent<Renderer>().material.color.r, detectionZone.GetComponent<Renderer>().material.color.g, detectionZone.GetComponent<Renderer>().material.color.b, 0.25f);
                    }
                    else {
                        userIsInRange = false;
                        detectionZone.GetComponent<Renderer>().material.color = new Color(detectionZone.GetComponent<Renderer>().material.color.r, detectionZone.GetComponent<Renderer>().material.color.g, detectionZone.GetComponent<Renderer>().material.color.b, 0.0f);
                    }
                }
                

                if (userIsInRange) {
                    lastUserTime = Time.realtimeSinceStartup;
                }

                bool bUserFound = (Time.realtimeSinceStartup - lastUserTime) < userLostMaxTime;

                if (!bUserFound)
                {
                    IsUserDetected = false;
                    statusText.text = string.Format("User Detected: " + IsUserDetected);
                }
                else if (bUserFound)
                {
                    kinectManager.ClearKinectUsers();
                    statusText.text = string.Format("User Detected: " + IsUserDetected);
                    IsUserDetected = true;
                }
            }
        }

        private IEnumerator DetectionConfidenceSmoothing(float n) {
            DOTween.To(() => userDetectionConfidence, x => userDetectionConfidence = x, n, 0.25f);
            yield return new WaitForSeconds(0.25f);
        }
    }
}
