﻿using UnityEngine;
using System.Collections;
using com.rfilkov.kinect;


namespace com.rfilkov.components
{
    /// <summary>
    /// PlayerDetectorController plays saved recording, when no user is detected for given amount of time.
    /// </summary>
    public class PlayerDetectorController : MonoBehaviour
    {
        [Tooltip("Depth sensor index - 0 is the 1st one, 1 - the 2nd one, etc.")]
        public int sensorIndex = 0;

        [Tooltip("Delay in seconds when the replay starts, after no more users have been detected.")]
        public float userLostMaxTime = 5f;

        public UnityEngine.UI.Text statusText;

        private KinectManager kinectManager = null;
        private KinectInterop.SensorData sensorData = null;

        private float lastUserTime = 0f;

        [SerializeField]
        private SOEvent_Bool so_userDetectionEvent;

        private bool isUserDetected = false;

        public bool IsUserDetected
        {
            get { return isUserDetected; }
            set
            {
                if (value == isUserDetected)
                    return;

                isUserDetected = value;
                if (isUserDetected)
                {
                    //if (transform.GetComponent<PlayerPositionBehavior>().userIsInRange) {

                        if (logConsole)
                        {
                            Debug.Log("User Found");
                        }

                        so_userDetectionEvent.Raise(true);
                    //}
                    
                }
                else {
                    if (logConsole) {
                        Debug.Log("User Lost");
                    }
                    
                    so_userDetectionEvent.Raise(false);
                }
            }
        }

        public bool logConsole = false;


        void Start()
        {
            kinectManager = KinectManager.Instance;

            sensorData = kinectManager != null ? kinectManager.GetSensorData(sensorIndex) : null;
        }

        void Update()
        {
            if (!kinectManager || sensorData == null)
                return;

            if (sensorData.trackedBodiesCount > 0)
            {
                lastUserTime = Time.realtimeSinceStartup;
            }

            bool bUserFound = (Time.realtimeSinceStartup - lastUserTime) < userLostMaxTime;

            if (!bUserFound)// && !transform.GetComponent<PlayerPositionBehavior>().userIsInRange)
            {
                IsUserDetected = false;
                statusText.text = string.Format("User Detected: " + IsUserDetected);
            }
            else if (bUserFound)
            {
                kinectManager.ClearKinectUsers();
                statusText.text = string.Format("User Detected: " + IsUserDetected);
                IsUserDetected = true;
            }
        }

    }
}
