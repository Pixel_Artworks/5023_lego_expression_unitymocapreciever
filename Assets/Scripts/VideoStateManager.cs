﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using System;

public class VideoStateManager : MonoBehaviour
{
    //[SerializeField]
    //private Canvas videoPlayerCanvas;

    private RenderHeads.Media.AVProVideo.Demos.SimpleController controller;
    private int index = 0;
    private int p_index = 0;

    //private string streamingAssetsFolder = 
    // Start is called before the first frame update
    void Start()
    {
        controller = transform.GetComponent<RenderHeads.Media.AVProVideo.Demos.SimpleController>();
        LoadClip(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadClip(int i) {
        index = i;
        if (index != p_index) {

            if (FindObjectOfType<CharacterSystem>().editMode)
                return;

            controller.LoadVideoExternal(controller._folder, i);
            
            p_index = index;
        }
    }
}
