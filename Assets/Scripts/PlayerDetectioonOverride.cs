﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetectioonOverride : MonoBehaviour
{
    public bool enableKienct = false;
    public bool triggerPlayerDetectionOnStart = false;

    [SerializeField]
    private GameObject kinectController;
    [SerializeField]
    private GameObject heightDetection;
    [SerializeField]
    private SOEvent_Bool so_userDetectionEvent;
    [SerializeField]
    private SOEvent_Bool so_setFacialTrackDeviceSelection;
    [SerializeField]
    private SOEvent_Bool so_setFaceIsTracked;
    // Start is called before the first frame update

    private void Awake()
    {
        kinectController.SetActive(enableKienct);
        heightDetection.SetActive(enableKienct);

    }

    private void Start()
    {
        StartCoroutine(DelayedStart());
    }

    private IEnumerator DelayedStart() {
        yield return new WaitForSeconds(0.5f);
        if (triggerPlayerDetectionOnStart)
        {
            so_userDetectionEvent.Raise(true);
            so_setFacialTrackDeviceSelection.Raise(true);
            //so_setFaceIsTracked.Raise(true);
        }
    }

}
