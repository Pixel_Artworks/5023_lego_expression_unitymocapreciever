﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TempAlphaSin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform. = _startPosition + new Vector3(Mathf.Sin(Time.time), 0.0f, 0.0f);
        transform.GetComponent<CanvasGroup>().alpha = Mathf.Sin(Time.time * 2f);
    }
}
