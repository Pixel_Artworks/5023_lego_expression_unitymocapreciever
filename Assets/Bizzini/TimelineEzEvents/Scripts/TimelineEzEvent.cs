﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Playables;

namespace Bizzini.TimelineEzEvents
{
    [Serializable]
    public class EzEvent
    {
        [NonSerialized] public PlayableDirector LinkedPlayableDirector;
        [NonSerialized] public TimelineEzEventsContainer LinkedEzEventsContainer;
        [NonSerialized] public TimelineEzEventsTrack LinkedEzTrack;
        [NonSerialized] public TimelineEzEventsClip LinkedEzClip;
        [NonSerialized] public double LastRaisedOnClipFrameId;
        [NonSerialized] public double LastRaisedOnDirectorTime = -1;
        public bool IsEnabled = true;
        public bool EvaluateInEditMode;
        public bool ConditionResultInverted;
        public bool ShowFirstComponentOnly = true;
        public bool ShowStaticMethods;
        public bool ShowBaseMethods;
        public bool ShowProperties;
        public bool ShowVoidOnlyReturnType = true;
        public MethodConfiguration[] MethodConfigurations = new MethodConfiguration[3];

        public enum OnClipInvocationTimeEnum { OnEnter, EachFrame };
        public OnClipInvocationTimeEnum OnClipInvocationTime;
        public enum OnFreeInvocationTimeEnum { ManualInvocation };

        public ObjectExposedReferenceManagement ObjectExposedReferenceManager;
        [NonSerialized] public TimelineEzEventsInvocationManagement InvocationManager;
        [NonSerialized] private Behaviour targetBehaviour;

        public EzEvent()
        {
            ObjectExposedReferenceManager = new ObjectExposedReferenceManagement();
            MethodConfigurations = new MethodConfiguration[3];
            LastRaisedOnDirectorTime = -1;
        }

        public EzEvent(TimelineEzEventsClip linkedToTimelineEventClip)
        {
            LinkedEzClip = linkedToTimelineEventClip;
            LinkedPlayableDirector = LinkedEzClip != null ? LinkedEzClip.LinkedPlayableDirector : null;
            ObjectExposedReferenceManager = new ObjectExposedReferenceManagement();
            MethodConfigurations = new MethodConfiguration[3];
            LastRaisedOnDirectorTime = -1;
        }

        public void Init(TimelineEzEventsContainer eventsContainer, PlayableDirector director, int eventIndex, TimelineEzEventsTrack track, TimelineEzEventsClip clip)
        {
            LinkedEzEventsContainer = eventsContainer;
            LinkedEzTrack = track;
            LinkedEzClip = clip;
            LinkedPlayableDirector = director;

            for (int methodIndex = 0; methodIndex < MethodConfigurations.Length; methodIndex++)
            {
                InitExposedReferences(director, eventIndex, methodIndex, clip);
                if (eventsContainer.TargetGameObject != null && (!string.IsNullOrEmpty(MethodConfigurations[methodIndex].HandlerKey)))
                {
                    if (methodIndex == 2 && string.IsNullOrEmpty(MethodConfigurations[0].HandlerKey))   //there's no condition method thus don't look inside False method
                        continue;

                    MethodConfigurations[methodIndex].InvocationData = !string.IsNullOrEmpty(MethodConfigurations[methodIndex].HandlerKey) ? TimelineEzEventsHelper.GetSuitableInvocationData(eventsContainer.TargetGameObject, methodIndex == 0 ? typeof(bool) : typeof(void), false, ShowFirstComponentOnly, ShowStaticMethods, ShowBaseMethods, ShowProperties, MethodConfigurations[methodIndex].HandlerKey).FirstOrDefault() : new TimelineEzEventsHelper.BehaviourMethodData();
                }
            }

            if (MethodConfigurations[1].InvocationData.HasData() || MethodConfigurations[2].InvocationData.HasData())
                InvocationManager = new TimelineEzEventsInvocationManagement(this, MethodConfigurations[0].InvocationData, MethodConfigurations[1].InvocationData, MethodConfigurations[2].InvocationData);
        }

        public void InitExposedReferences(PlayableDirector director, int eventIndex, int methodIndex, TimelineEzEventsClip clip = null)
        {
#if UNITY_EDITOR
            ObjectExposedReferenceManager.ClearUnusedExposedObjects(director, this, methodIndex);
#endif
            for (int exposedInfoIndex = 0; exposedInfoIndex < ObjectExposedReferenceManager.GetExposedInfoCount(methodIndex); exposedInfoIndex++)
            {
                string argObjExposedName = null;
                ObjectExposedReferenceManagement.ObjectExposedInfo objRefExposedInfo = ObjectExposedReferenceManager.Get(methodIndex, exposedInfoIndex);
                int argIndex = -1;
                if (objRefExposedInfo != null)
                {
                    objRefExposedInfo.UpdateMatch(clip, methodIndex, eventIndex);
                    argObjExposedName = objRefExposedInfo.Name;
                    eventIndex = objRefExposedInfo.EventIndex;
                    argIndex = objRefExposedInfo.ArgIndex;
                }
                if (!string.IsNullOrEmpty(argObjExposedName))
                {
                    objRefExposedInfo = ObjectExposedReferenceManager.Set(argObjExposedName, exposedInfoIndex, eventIndex, methodIndex, argIndex, objRefExposedInfo.ResolvedObj);
                    objRefExposedInfo.Store(director, objRefExposedInfo.ResolvedObj == null);
                }
            }
        }

        public object Invoke(FrameData frameData, double curTime = 0d)
        {
            if (InvocationManager != null)
            {
                LastRaisedOnClipFrameId = frameData.frameId;
                LastRaisedOnDirectorTime = curTime;
                InvocationManager.CurrentFrameData = frameData;
                return InvocationManager.Invoke();
            }
            return null;
        }

        [Serializable]
        public class MethodConfiguration
        {
            public string HandlerKey;
            public string[] ArgValues;
#if UNITY_EDITOR
            public string[] ArgTypes;
#endif
            [SerializeField] private TimelineEzEventsHelper.BehaviourMethodData _invocationData;

            public TimelineEzEventsHelper.BehaviourMethodData InvocationData
            {
                get
                {
                    return _invocationData;
                }

                set
                {
                    _invocationData = value;
                }
            }

#if UNITY_EDITOR
            public string CreateStringSnapshot(EzEvent ezEvent, int methodIndex)
            {
                string stringSnapshot;
                TimelineEzEventsHelper.MethodNameArgsStruct methodNameStruct = new TimelineEzEventsHelper.MethodNameArgsStruct(HandlerKey);
                StringBuilder sb = new StringBuilder();
                sb.Append("<b>" + methodNameStruct.MethodNameNoArgs + "()</b> ");
                try
                {
                    if (ArgTypes != null)
                    {
                        int argsObjCnt = 0;
                        for (int i = 0; i < ArgTypes.Length; i++)
                        {
                            if (i > 0)
                                sb.Append(" , ");
                            if (i < ArgValues.Length)
                            {
                                if (ArgTypes[i] == "Object")
                                {
                                    ObjectExposedReferenceManagement.ObjectExposedInfo objExposedInfo = ezEvent.ObjectExposedReferenceManager.Get(methodIndex, argsObjCnt);
                                    sb.Append(ArgTypes[i] + "=<b>" + (objExposedInfo != null && objExposedInfo.ResolvedObj != null ? objExposedInfo.ResolvedObj.ToString() : "null") + "</b>");
                                    argsObjCnt++;
                                }
                                else
                                    sb.Append(ArgTypes[i] + "=<b>" + ArgValues[i] + "</b>");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e);
                }
                finally
                {
                    stringSnapshot = sb.ToString();
                }
                return stringSnapshot;
            }

            public int CountArgObjects()
            {
                int cnt = 0;
                if (ArgTypes != null)
                {
                    foreach (string type in ArgTypes)
                    {
                        if (type == "Object")
                            cnt++;
                    }
                }
                return cnt;
            }
#endif
        }

        [Serializable]
        public class ObjectExposedReferenceManagement
        {
            [SerializeField] private MethodsObjectExposedInfo[] methodsExposedInfo = new MethodsObjectExposedInfo[3];

            public ObjectExposedReferenceManagement()
            {
                methodsExposedInfo = new MethodsObjectExposedInfo[3];
            }

            [Serializable]
            public class MethodsObjectExposedInfo
            {
                public List<ObjectExposedInfo> InfoList;

                public MethodsObjectExposedInfo()
                {
                    InfoList = new List<ObjectExposedInfo>();
                }
            }

            public bool Store(int methodIndex, int index, PlayableDirector director, bool store)
            {
                ObjectExposedInfo info = index < methodsExposedInfo[methodIndex].InfoList.Count ? methodsExposedInfo[methodIndex].InfoList[index] : null;
                if (info != null)
                    return info.Store(director, store);
                return false;
            }

            public ObjectExposedInfo Get(int methodIndex, int index)
            {
                return index < methodsExposedInfo[methodIndex].InfoList.Count ? methodsExposedInfo[methodIndex].InfoList[index] : null;
            }

            public List<ObjectExposedInfo> Get(int methodIndex)
            {
                return methodsExposedInfo[methodIndex].InfoList;
            }

            public ObjectExposedInfo Set(string name, int index, int eventIndex, int methodIndex, int argIndex, UnityEngine.Object obj = null)
            {
                resizeObjectExposedArray(methodIndex, index);
                methodsExposedInfo[methodIndex].InfoList[index].Reference.exposedName = methodsExposedInfo[methodIndex].InfoList[index].Name = name;
                methodsExposedInfo[methodIndex].InfoList[index].EventIndex = eventIndex;
                methodsExposedInfo[methodIndex].InfoList[index].ArgIndex = argIndex;
                methodsExposedInfo[methodIndex].InfoList[index].ResolvedObj = obj;
                return methodsExposedInfo[methodIndex].InfoList[index];
            }

            public int GetMethodsExposedCount()
            {
                return methodsExposedInfo != null ? methodsExposedInfo.Length : 0;
            }

            public int GetExposedInfoCount(int methodIndex)
            {
                return methodIndex < methodsExposedInfo.Length && methodsExposedInfo[methodIndex] != null ? methodsExposedInfo[methodIndex].InfoList.Count : 0;
            }

            private void resizeObjectExposedArray(int methodIndex, int index)
            {
                if (index >= methodsExposedInfo[methodIndex].InfoList.Count)
                {
                    int diff = (index + 1) - methodsExposedInfo[methodIndex].InfoList.Count;
                    for (int i = 0; i < diff; i++)
                        methodsExposedInfo[methodIndex].InfoList.Add(new ObjectExposedInfo());
                }
            }

#if UNITY_EDITOR
            public void ClearUnusedExposedObjects(PlayableDirector director, EzEvent ezEvent, int methodIndex)
            {
                int objectsCount = ezEvent.MethodConfigurations[methodIndex].CountArgObjects();
                bool clearedSomething = false;
                for (int i = objectsCount; i < methodsExposedInfo[methodIndex].InfoList.Count; i++)
                {
                    if (methodsExposedInfo[methodIndex].InfoList[i] != null)
                    {
                        bool wasValid = false;
                        PropertyName exposedName = methodsExposedInfo[methodIndex].InfoList[i].Reference.exposedName;
                        if (director != null)
                        {
                            director.GetReferenceValue(exposedName, out wasValid);
                            director.ClearReferenceValue(exposedName);
                        }
                        if (wasValid)
                            clearedSomething = true;
                        methodsExposedInfo[methodIndex].InfoList[i] = null;
                    }
                }
                if (!Application.isPlaying && clearedSomething && director != null)
                {
                    UnityEditor.EditorUtility.SetDirty(director);
                    UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(director.gameObject.scene);
                }
            }

            public void Clear(PlayableDirector director, int methodIndex)
            {
                for (int i = 0; i < GetExposedInfoCount(methodIndex); i++)
                {
                    ObjectExposedInfo objExposedInfo = Get(methodIndex, i);
                    if (director != null && objExposedInfo != null)
                        director.ClearReferenceValue(objExposedInfo.Name);
                }
            }

            public void Clear(PlayableDirector director)
            {
                for (int methodIndex = 0; methodIndex < GetMethodsExposedCount(); methodIndex++)
                    Clear(director, methodIndex);
            }
#endif

            [Serializable]
            public class ObjectExposedInfo
            {
                public string Name;
                public int EventIndex;
                public int ArgIndex;
                public ExposedReference<UnityEngine.Object> Reference;
                public UnityEngine.Object ResolvedObj;

                public void UpdateMatch(TimelineEzEventsClip clip, int methodIndex, int eventIndex)
                {
                    EventIndex = eventIndex;
                    Name = TimelineEzEventsHelper.GenerateObjectReferenceExposedName(clip, EventIndex, methodIndex, ArgIndex);
                }

                public UnityEngine.Object Resolve(PlayableDirector director)
                {
                    ResolvedObj = Reference.Resolve(director);
                    return ResolvedObj;
                }

                public bool ObjIsStored(PlayableDirector director, string name)
                {
                    if (director != null && !string.IsNullOrEmpty(Name))
                    {
                        bool idValid = false;
                        director.GetReferenceValue(name, out idValid);
                        return idValid;
                    }
                    return false;
                }

                public bool Store(PlayableDirector director, bool resolve)
                {
                    if (director != null && !string.IsNullOrEmpty(Name))
                    {
                        UnityEngine.Object obj = resolve ? Resolve(director) : ResolvedObj;
                        //if (obj == null)
                        //    director.ClearReferenceValue(Name);
                        //else
                        director.SetReferenceValue(Name, obj);
                        return true;
                    }
                    return false;
                }
            }
        }

#if UNITY_EDITOR
        [NonSerialized] public Rect GuiBounds;
        [NonSerialized] public bool GuiIsDragging;

        public void ToClipboard()
        {
            ClipboardObject clipboardObject = new ClipboardObject();
            clipboardObject.IsEnabled = IsEnabled;
            clipboardObject.OnClipInvocationTime = OnClipInvocationTime;
            clipboardObject.EvaluateInEditMode = EvaluateInEditMode;
            clipboardObject.ConditionResultInverted = ConditionResultInverted;
            clipboardObject.ShowFirstComponentOnly = ShowFirstComponentOnly;
            clipboardObject.ShowStaticMethods = ShowStaticMethods;
            clipboardObject.ShowBaseMethods = ShowBaseMethods;
            clipboardObject.ShowProperties = ShowProperties;
            clipboardObject.ShowVoidOnlyReturnType = ShowVoidOnlyReturnType;
            Array.Copy(MethodConfigurations, clipboardObject.MethodConfigurations, MethodConfigurations.Length);
            for (int methodIndex = 0; methodIndex < MethodConfigurations.Length; methodIndex++)
            {
                clipboardObject.MethodsObjectExposedInfoArr[methodIndex] = new ObjectExposedReferenceManagement.MethodsObjectExposedInfo();
                clipboardObject.MethodsObjectExposedInfoArr[methodIndex].InfoList = new List<ObjectExposedReferenceManagement.ObjectExposedInfo>(ObjectExposedReferenceManager.Get(methodIndex));
            }
            UnityEditor.EditorGUIUtility.systemCopyBuffer = JsonUtility.ToJson(clipboardObject);
        }

        public void FromClipboard()
        {
            try
            {
                ClipboardObject clipboardObject = JsonUtility.FromJson<ClipboardObject>(UnityEditor.EditorGUIUtility.systemCopyBuffer);
                IsEnabled = clipboardObject.IsEnabled;
                OnClipInvocationTime = clipboardObject.OnClipInvocationTime;
                EvaluateInEditMode = clipboardObject.EvaluateInEditMode;
                ConditionResultInverted = clipboardObject.ConditionResultInverted;
                ShowFirstComponentOnly = clipboardObject.ShowFirstComponentOnly;
                ShowStaticMethods = clipboardObject.ShowStaticMethods;
                ShowBaseMethods = clipboardObject.ShowBaseMethods;
                ShowProperties = clipboardObject.ShowProperties;
                ShowVoidOnlyReturnType = clipboardObject.ShowVoidOnlyReturnType;
                Array.Copy(clipboardObject.MethodConfigurations, MethodConfigurations, clipboardObject.MethodConfigurations.Length);
                for (int methodIndex = 0; methodIndex < clipboardObject.MethodConfigurations.Length; methodIndex++)
                {
                    ObjectExposedReferenceManager.Clear(LinkedPlayableDirector, methodIndex);
                    for (int i = 0; i < clipboardObject.MethodsObjectExposedInfoArr[methodIndex].InfoList.Count; i++)
                    {
                        ObjectExposedReferenceManagement.ObjectExposedInfo item = (ObjectExposedReferenceManagement.ObjectExposedInfo)clipboardObject.MethodsObjectExposedInfoArr[methodIndex].InfoList[i];
                        ObjectExposedReferenceManager.Set(item.Name, i, item.EventIndex, methodIndex, item.ArgIndex, item.ResolvedObj);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        [Serializable]
        public class ClipboardObject
        {
            public bool IsEnabled;
            public OnClipInvocationTimeEnum OnClipInvocationTime;
            public bool EvaluateInEditMode;
            public bool ConditionResultInverted;
            public MethodConfiguration[] MethodConfigurations = new MethodConfiguration[3];
            public bool ShowFirstComponentOnly;
            public bool ShowStaticMethods;
            public bool ShowBaseMethods;
            public bool ShowProperties;
            public bool ShowVoidOnlyReturnType;
            public string[] ArgValues;
            public ObjectExposedReferenceManagement.MethodsObjectExposedInfo[] MethodsObjectExposedInfoArr = new ObjectExposedReferenceManagement.MethodsObjectExposedInfo[3];
        }
#endif
    }
}