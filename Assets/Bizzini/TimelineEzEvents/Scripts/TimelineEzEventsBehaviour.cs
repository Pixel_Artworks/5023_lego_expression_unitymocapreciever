using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Bizzini.TimelineEzEvents
{
    [Serializable]
    public class TimelineEzEventsBehaviour : PlayableBehaviour
    {
        public TimelineEzEventsContainer EventsContainer;
        [NonSerialized] public TimelineClip LinkedClip;
        [NonSerialized] public TimelineEzEventsClip LinkedEzClip;

        public override void OnPlayableCreate(Playable playable)
        {
            base.OnPlayableCreate(playable);
            if (EventsContainer == null)
                EventsContainer = new TimelineEzEventsContainer();
            if (EventsContainer.Events == null)
                EventsContainer.Events = new EzEvent[0];
        }
    }
}