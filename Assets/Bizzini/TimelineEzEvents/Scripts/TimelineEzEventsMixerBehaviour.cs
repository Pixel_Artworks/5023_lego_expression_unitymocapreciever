using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Bizzini.TimelineEzEvents
{
    public class TimelineEzEventsMixerBehaviour : PlayableBehaviour
    {
        [NonSerialized] public TimelineEzEventsTrack LinkedToTrack;
        [NonSerialized] public int LastInvokedClipIndex = -1;
        [NonSerialized] public int? TimeJumpNewLastClipIndex = null;
        private PlayableDirector director;
        private double directorLastTime;

        public override void OnPlayableCreate(Playable playableTrack)
        {
            base.OnPlayableCreate(playableTrack);
            director = playableTrack.GetGraph().GetResolver() as PlayableDirector;

            DoReset();
        }

        private void DoReset()
        {
            LastInvokedClipIndex = -1;
            TimeJumpNewLastClipIndex = null;
        }

        public override void ProcessFrame(Playable playableTrack, FrameData info, object playerData)
        {
            base.ProcessFrame(playableTrack, info, playerData);
            TimelineEzEventsBehaviour curActiveBehaviour = null;

            if (TimeJumpNewLastClipIndex.HasValue)
                LastInvokedClipIndex = TimeJumpNewLastClipIndex.Value;

            if (director.time < directorLastTime && !TimeJumpNewLastClipIndex.HasValue) //timeline looped
            {
                handleSkippedClips(director.duration, true, playableTrack, info);
                DoReset();
            }

            curActiveBehaviour = handleSkippedClips(director.time, false, playableTrack, info);

            if (curActiveBehaviour != null)
                InvokeClipEvents(curActiveBehaviour, info); //invoke current clip events
            else if (!Application.isPlaying)
                LastInvokedClipIndex = -1;

            directorLastTime = director.time;
            TimeJumpNewLastClipIndex = null;
        }

        private TimelineEzEventsBehaviour handleSkippedClips(double curTime, bool justLooped, Playable playableTrack, FrameData info)
        {
            int clipsCount = playableTrack.GetInputCount();
            TimelineEzEventsBehaviour curActiveBehaviour = null;
            bool timeSyncedToClipStart = false;
            for (int i = 0; i < clipsCount; i++)
            {
                float inputWeight = playableTrack.GetInputWeight(i);
                ScriptPlayable<TimelineEzEventsBehaviour> inputPlayable = (ScriptPlayable<TimelineEzEventsBehaviour>)playableTrack.GetInput(i);
                TimelineEzEventsBehaviour behaviour = inputPlayable.GetBehaviour();
                TimelineEzEventsClip ezClip = behaviour.LinkedEzClip;
                if (!timeSyncedToClipStart && inputWeight == 1)
                    curActiveBehaviour = behaviour;

                if (Application.isPlaying && director.state == PlayState.Playing && LastInvokedClipIndex < ezClip.IndexOnTrack && ((!justLooped && curTime > ezClip.LinkedTimelineClip.end) || (justLooped && (curTime > ezClip.LinkedTimelineClip.end || Mathf.Approximately((float)curTime, (float)ezClip.LinkedTimelineClip.end)))))
                {
                    Debugging.TimelineEzEventsDebugger.ConsoleLogClipSkipped(director.name, behaviour.LinkedClip.displayName);
                    timeSyncedToClipStart = InvokeClipEvents(behaviour, info);
                    if (timeSyncedToClipStart)  //invoke past skipped events
                    {
                        curActiveBehaviour = behaviour;
                        curTime = director.time;
                    }
                }
            }
            return curActiveBehaviour;
        }

        public bool InvokeClipEvents(TimelineEzEventsBehaviour behaviour, FrameData info)
        {
            bool timeSyncedToClipStart = false;
            if (Application.isPlaying && director.state == PlayState.Playing && behaviour.EventsContainer.ClipTimeSnap && behaviour.LinkedEzClip.IndexOnTrack != LastInvokedClipIndex)
            {
                director.time = behaviour.LinkedClip.start;   //avoid next skipped event execution if Timeline time is modified
                timeSyncedToClipStart = true;
                for (int i = 0; i < LinkedToTrack.timelineAsset.outputTrackCount; i++)  
                {
                    TimelineEzEventsTrack ezTrack = LinkedToTrack.timelineAsset.GetOutputTrack(i) as TimelineEzEventsTrack;
                    if (ezTrack != null && ezTrack != LinkedToTrack)
                        ezTrack.MixerBehaviour.directorLastTime = director.time;    //prevent other tracks from detecting a wrong timeline loop
                }
            }

            for (int i = 0; i < behaviour.EventsContainer.Events.Length; i++)
                if (behaviour.EventsContainer.Events[i].IsEnabled)
                    if (LastInvokedClipIndex != behaviour.LinkedEzClip.IndexOnTrack || (TimeJumpNewLastClipIndex.HasValue && LastInvokedClipIndex == TimeJumpNewLastClipIndex) || behaviour.EventsContainer.Events[i].OnClipInvocationTime == EzEvent.OnClipInvocationTimeEnum.EachFrame)
                        if (Application.isPlaying || behaviour.EventsContainer.Events[i].EvaluateInEditMode)
                            behaviour.EventsContainer.Events[i].Invoke(info, director.time);

            LastInvokedClipIndex = !TimeJumpNewLastClipIndex.HasValue ? behaviour.LinkedEzClip.IndexOnTrack : TimeJumpNewLastClipIndex.Value;
            return timeSyncedToClipStart;
        }
    }
}
