﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bizzini.TimelineEzEvents
{
    public class TimelineEzEventsConfigurationAsset : ScriptableObject
    {
        public bool LogMethodCalls = true;
        public bool LossMethodProtection = true;
        public bool GroupMethodsByUnderscore = true;
        [Multiline]
        public string SearchTypes;
        public bool SearchInvalidMethodsOnly = true;
        public string SearchMethodName;
    }
}
