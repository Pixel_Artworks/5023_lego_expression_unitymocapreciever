﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Bizzini.TimelineEzEvents
{
    public static class TimelineEzEventsHelper
    {
        public const string ComponentExposedReferencePrefix = "TimelineEzObjRef";
        public const string ClipExposedReferencePrefix = "TimelineEzClipObjRef";

        public static string GenerateObjectReferenceExposedName(TimelineEzEventsClip clip, int eventIndex, int methodIndex, int argObjIndex)
        {
            return (clip != null ? ClipExposedReferencePrefix + "-" + clip.UniqueId : ComponentExposedReferencePrefix) + "(" + eventIndex + "," + methodIndex + "," + argObjIndex + ")";
        }

        public static void AssignTracksIndex(TimelineAsset timelineAsset)
        {
            int cnt = 0;
            for (int i = 0; i < timelineAsset.outputTrackCount; i++)
            {
                TimelineEzEventsTrack ezEventTrack = timelineAsset.GetOutputTrack(i) as TimelineEzEventsTrack;
                if (ezEventTrack != null)
                {
                    ezEventTrack.name = "EzEventsTrack_" + cnt;
                    ezEventTrack.PrevIndexOnTimeline = ezEventTrack.IndexOnTimeline;
                    ezEventTrack.IndexOnTimeline = cnt;
                    cnt++;
                }
            }
        }

        public static TimelineClip GetTimelineClip(TimelineAsset timelineAsset, TimelineEzEventsClip ezClip)
        {
            if (timelineAsset != null && ezClip != null)
            {
                for (int i = 0; i < timelineAsset.outputTrackCount; i++)
                {
                    TimelineEzEventsTrack ezEventTrack = timelineAsset.GetOutputTrack(i) as TimelineEzEventsTrack;
                    if (ezEventTrack != null)
                    {
                        foreach (TimelineClip tclip in ezEventTrack.GetClips())
                        {
                            if (tclip.asset != null)
                            {
                                TimelineEzEventsClip ezc = tclip.asset as TimelineEzEventsClip;
                                if (ezc != null && ezc == ezClip)
                                    return tclip;
                            }
                        }
                    }
                }
            }
            return null;
        }

        public static void TimelineGoToTime(PlayableDirector director, double seconds, bool relative)
        {
            if (relative)
                director.time += seconds;
            else
                director.time = seconds;
            OnTimelineTimeJump(director, director.time);
        }

        public static void OnTimelineTimeJump(PlayableDirector director, double newTime)
        {
            TimelineAsset timelineAsset = director.playableAsset as TimelineAsset;
            for (int i = 0; i < timelineAsset.outputTrackCount; i++)
            {
                TimelineEzEventsTrack ezEventTrack = timelineAsset.GetOutputTrack(i) as TimelineEzEventsTrack;
                if (ezEventTrack != null && !ezEventTrack.muted)
                    if (ezEventTrack != null)
                        ezEventTrack.OnTimeJump(newTime);
            }
        }

        public static TimelineEzEventsClip GetPreviousEzClipInTimeline(TimelineEzEventsTrack track, double fromTime)
        {
            TimelineClip prevClip = null;
            float closestTime = -1;
            float t = (float)fromTime;
            foreach (TimelineClip clip in track.GetClips())
            {
                float clipStart = (float)clip.start;
                float clipEnd = (float)clip.end;
                if (clipStart < t && clipEnd < t)
                {
                    if (clipStart >= closestTime)
                    {
                        prevClip = clip;
                        closestTime = clipEnd;
                    }
                }
            }
            return prevClip != null ? prevClip.asset as TimelineEzEventsClip : null;
        }

        public static TimelineEzEventsClip GetNextEzClipInTimeline(TimelineEzEventsTrack track, double fromTime, bool closestTimeMode = true)
        {
            TimelineClip nextClip = null;
            double closestTime = double.MaxValue;
            double farthestTime = 0;
            foreach (TimelineClip clip in track.GetClips())
            {
                if (clip.start > fromTime)
                {
                    if (closestTimeMode)
                    {
                        if (clip.start <= closestTime)
                        {
                            nextClip = clip;
                            closestTime = clip.start;
                        }
                    }
                    else if (clip.start >= farthestTime)
                    {
                        nextClip = clip;
                        farthestTime = clip.start;
                    }
                }
            }
            return nextClip != null ? nextClip.asset as TimelineEzEventsClip : null;
        }

        public static TimelineEzEventsClip GetPreviousEzClipInTimeline(PlayableDirector director, double fromTime)
        {
            TimelineClip prevClip = null;
            double closestTime = -1;
            foreach (PlayableBinding o in director.playableAsset.outputs)
            {
                if (o.sourceObject is TrackAsset)
                {
                    TrackAsset track = o.sourceObject as TrackAsset;
                    foreach (TimelineClip clip in track.GetClips())
                    {
                        if (clip.start < fromTime)
                        {
                            if (clip.start > closestTime)
                            {
                                prevClip = clip;
                                closestTime = clip.end;
                            }
                        }
                    }
                }
            }
            return prevClip != null ? prevClip.asset as TimelineEzEventsClip : null;
        }

        public static TimelineEzEventsClip GetNextEzClipInTimeline(PlayableDirector director, double fromTime, bool closestTimeMode = true)
        {
            TimelineClip nextClip = null;
            double closestTime = double.MaxValue;
            double farthestTime = 0;
            foreach (PlayableBinding o in director.playableAsset.outputs)
            {
                if (o.sourceObject is TrackAsset)
                {
                    TrackAsset track = o.sourceObject as TrackAsset;
                    foreach (TimelineClip clip in track.GetClips())
                    {
                        if (clip.start > fromTime)
                        {
                            if (closestTimeMode)
                            {
                                if (clip.start < closestTime)
                                {
                                    nextClip = clip;
                                    closestTime = clip.start;
                                }
                            }
                            else if (clip.start > farthestTime)
                            {
                                nextClip = clip;
                                farthestTime = clip.start;
                            }
                        }
                    }
                }
            }
            return nextClip != null ? nextClip.asset as TimelineEzEventsClip : null;
        }

#if UNITY_EDITOR
        public static EzEvent[] AddEvent(EzEvent[] events)
        {
            Array.Resize(ref events, events.Length + 1);
            return events;
        }

        public static EzEvent[] DeleteEvent(EzEvent[] events, PlayableDirector director, int eventIndex)
        {
            if (events != null)
            {
                if (eventIndex < events.Length)
                    events[eventIndex].ObjectExposedReferenceManager.Clear(director);
                if (eventIndex + 1 < events.Length)
                    for (int i = eventIndex + 1; i < events.Length; i++)
                        if (eventIndex < events.Length)
                            events[eventIndex].ObjectExposedReferenceManager.Clear(director);

                events = events.RemoveAt(eventIndex);

                for (int i = 0; i < events.Length; i++)
                {
                    if (events[i].LinkedEzTrack == null)  //events inside clip will have this method called on CreatePlayable
                    {
                        events[i].InitExposedReferences(director, i, 0);
                        events[i].InitExposedReferences(director, i, 1);
                        events[i].InitExposedReferences(director, i, 2);
                    }
                }
            }
            return events;
        }

        public static EzEvent[] SwapEvent(EzEvent[] events, PlayableDirector director, int curIndex, int newIndex)
        {
            events = events.SwapElement(curIndex, newIndex);
            for (int i = 0; i < events.Length; i++)
            {
                if (events[i].LinkedEzTrack == null)  //events inside clip will have this method called on CreatePlayable
                {
                    events[i].InitExposedReferences(director, i, 0);
                    events[i].InitExposedReferences(director, i, 1);
                    events[i].InitExposedReferences(director, i, 2);
                }
            }
            return events;
        }
#endif
        public static string GetSubstringByDelimiters(string delA, string delB, string text, int offset = 0)
        {
            if (!string.IsNullOrEmpty(text))
            {
                if (!string.IsNullOrEmpty(delA) && !string.IsNullOrEmpty(delB))
                {
                    if (text.IndexOf(delA) > -1 && text.IndexOf(delB) > -1)
                    {
                        int del_A_endPos = text.IndexOf(delA, offset) + delA.Length;
                        int del_B_startPos = text.IndexOf(delB, del_A_endPos);
                        string t = text.Substring((del_A_endPos), (del_B_startPos - del_A_endPos));
                        return t;
                    }
                }
            }
            return "";
        }

        public static List<BehaviourMethodData> GetSuitableInvocationData(GameObject gameObject, Type returnType, bool noArguments, bool firstComponent, bool staticMethods, bool baseMethods, bool properties, string fullhandlerKey = null)
        {
            MethodNameArgsStruct methodNameStruct = new MethodNameArgsStruct(fullhandlerKey);
            List<UnityEngine.Object> objects = gameObject != null ? gameObject.GetComponents<Component>().ToList<UnityEngine.Object>() : new List<UnityEngine.Object>(0);
            if (!firstComponent)
                objects.Add(gameObject);
            List<BehaviourMethodData> invocationData = new List<BehaviourMethodData>(objects.Count);

            Func<MethodInfo, bool> predicate = x => ((returnType == null || x.ReturnType == returnType) && (!noArguments || x.GetParameters().Count() > 0) && (properties || !x.IsSpecialName) && (TimelineEzEventsInvocationManagement.IsArgOfSupportedType(x.GetParameters())));

            int cnt = 0;
            foreach (UnityEngine.Object obj in objects)
            {
                if (!firstComponent || cnt == 1)    //the first is Transform
                {
                    if (methodNameStruct.MethodNameNoArgs == null)
                    {
                        List<MethodInfo> methodInfoList = obj.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance | (staticMethods ? BindingFlags.Static : 0) | (!baseMethods ? BindingFlags.DeclaredOnly : 0))
                            .Where(predicate)
                            .ToList();

                        if (methodInfoList.Count > 0)
                            invocationData.Add(new BehaviourMethodData(obj, methodInfoList));
                    }
                    else if (methodNameStruct.TargetMethodArgTypes != null)
                    {
#if UNITY_EDITOR
                        bool foundNullArgType = false;
                        for (int i = 0; i < methodNameStruct.TargetMethodArgTypes.Length; i++)
                        {
                            if (methodNameStruct.TargetMethodArgTypes[i] == null)
                            {
                                foundNullArgType = true;
                                break;
                            }
                        }
                        if (foundNullArgType)
                            continue;
#endif
                        MethodInfo foundMethodInfo = obj.GetType().GetMethod(methodNameStruct.TargetMethodName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | (!baseMethods ? BindingFlags.DeclaredOnly : 0), null, methodNameStruct.TargetMethodArgTypes, null);
                        if (foundMethodInfo != null)
                        {
                            if (foundMethodInfo.DeclaringType.FullName == methodNameStruct.DeclaringType)
                            {
                                invocationData.Add(new BehaviourMethodData(obj, new List<MethodInfo>(1) { foundMethodInfo }));
                                break;
                            }
                        }
                    }
                }
                cnt++;
            }
#if UNITY_EDITOR
            if (fullhandlerKey != null)
            {
                if (invocationData.Count <= 0)
                    Debugging.TimelineEzEventsDebugger.ConsoleLogLostMethod(false, gameObject, methodNameStruct.GetFullName());
            }
#endif
            return invocationData;
        }

        public static string MethodHandlerKeyNameSimilarityLostLookup(List<BehaviourMethodData> behaviourMethodDataList, MethodNameArgsStruct methodNameStruct)
        {
            string bestMethodName = null;
            string bestHandlerArgs = null;

            if (!string.IsNullOrEmpty(methodNameStruct.MethodNameNoArgs))
            {
                int minorDistance = int.MaxValue;
                foreach (BehaviourMethodData methodData in behaviourMethodDataList)
                {
                    foreach (MethodInfo methodInfo in methodData.MethodInfos)
                    {
                        string methodCompleteName = methodInfo.DeclaringType.ToString() + "." + methodInfo.Name;
                        int dist = stringLevenshteinDistanceCompute(methodNameStruct.MethodNameNoArgs, methodCompleteName);
                        if (dist < minorDistance)
                        {
                            bestMethodName = methodCompleteName;
                            minorDistance = dist;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(bestMethodName))
                {
                    minorDistance = int.MaxValue;
                    foreach (BehaviourMethodData methodData in behaviourMethodDataList)
                    {
                        foreach (MethodInfo methodInfo in methodData.MethodInfos)
                        {
                            string methodCompleteName = methodInfo.DeclaringType.ToString() + "." + methodInfo.Name;
                            if (methodCompleteName == bestMethodName)
                            {
                                string argsCSV = methodInfo.GetParamatersAsCSV();
                                int dist = stringLevenshteinDistanceCompute(methodNameStruct.ArgumentsCSV, argsCSV);
                                if (dist < minorDistance)
                                {
                                    bestHandlerArgs = argsCSV;
                                    minorDistance = dist;
                                }
                            }
                        }
                    }
                }
            }
            return bestMethodName + MethodNameArgsStruct.GetArgsWithDelimiters(bestHandlerArgs);
        }

        public struct MethodNameArgsStruct
        {
            public string MethodNameNoArgs;
            public string ArgumentsCSV;
            public string DeclaringType;
            public string TargetMethodName;
            public Type[] TargetMethodArgTypes;

            public MethodNameArgsStruct(string methodFullName)
            {
                MethodNameNoArgs = methodFullName;
                ArgumentsCSV = GetParametersFromFullName(methodFullName);
                MethodNameNoArgs = GetMethodNameNoArgsFromFull(methodFullName);
                DeclaringType = null;
                TargetMethodName = null;
                TargetMethodArgTypes = null;

                if (!string.IsNullOrEmpty(MethodNameNoArgs))
                {
                    Match match = Regex.Match(MethodNameNoArgs, @"[^\.]+$", RegexOptions.RightToLeft);
                    if (match != null)
                    {
                        TargetMethodName = match.Value;
                        if (!string.IsNullOrEmpty(ArgumentsCSV))
                        {
                            string[] typeNames = ArgumentsCSV.Split(',');
                            TargetMethodArgTypes = new Type[typeNames.Length];
                            for (int i = 0; i < typeNames.Length; i++)
                                TargetMethodArgTypes[i] = typeNames[i].GetEzType();
                        }
                        else
                            TargetMethodArgTypes = new Type[0];
                        DeclaringType = MethodNameNoArgs.Replace("." + match.Value, "");
                    }
                }
            }

            public string GetFullName()
            {
                return MethodNameNoArgs + "(" + ArgumentsCSV + ")";
            }

            public static string GetArgsWithDelimiters(string argsCSV)
            {
                return "(" + argsCSV + ")";
            }

            public static string GetParametersFromFullName(string methodFullName)
            {
                return GetSubstringByDelimiters("(", ")", methodFullName);
            }

            public static string GetMethodNameNoArgsFromFull(string methodFullName)
            {
                string handlerKeyArgs = GetParametersFromFullName(methodFullName);
                return !string.IsNullOrEmpty(methodFullName) ? methodFullName.Replace("(" + handlerKeyArgs + ")", "") : null;
            }
        }

        private static int stringLevenshteinDistanceCompute(string s, string t)
        {
            if (string.IsNullOrEmpty(s))
            {
                if (string.IsNullOrEmpty(t))
                    return 0;
                return t.Length;
            }

            if (string.IsNullOrEmpty(t))
            {
                return s.Length;
            }

            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // initialize the top and right of the table to 0, 1, 2, ...
            for (int i = 0; i <= n; d[i, 0] = i++) ;
            for (int j = 1; j <= m; d[0, j] = j++) ;

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                    int min1 = d[i - 1, j] + 1;
                    int min2 = d[i, j - 1] + 1;
                    int min3 = d[i - 1, j - 1] + cost;
                    d[i, j] = Math.Min(Math.Min(min1, min2), min3);
                }
            }
            return d[n, m];
        }

        public struct BehaviourMethodData
        {
            public UnityEngine.Object TargetObject;
            public List<MethodInfo> MethodInfos;

            public BehaviourMethodData(UnityEngine.Object targetObject, List<MethodInfo> methodInfos)
            {
                TargetObject = targetObject;
                MethodInfos = methodInfos;
            }

            public int GetMethodsCount()
            {
                return MethodInfos != null ? MethodInfos.Count : 0;
            }

            public bool HasData()
            {
                return (TargetObject != null && MethodInfos.Count > 0);
            }
        }

        public static string MethodArgsToPrettyString(object[] args)
        {
            string s = "";
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] != null)
                {
                    if (i > 0)
                        s += "<b> , </b>";
                    s += "(" + args[i].GetType() + ")" + (object)args[i];
                }
            }
            return s;
        }

        public static int IndexOfMethodName(string[] arr, string methodNameFullWithArgs)
        {
            int index = -1;
            for (int i = 0; i < arr.Length; i++)
                if (arr[i] == methodNameFullWithArgs)
                    return i;

            return index;
        }

        public static Type FindType(string qualifiedTypeName)
        {
            Type t = Type.GetType(qualifiedTypeName);

            if (t != null)
            {
                return t;
            }
            else
            {
                foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
                {
                    t = asm.GetType(qualifiedTypeName);
                    if (t != null)
                        return t;
                }
                return null;
            }
        }

#if UNITY_EDITOR
        public static string GetNameWithParent(Component component)
        {
            string s = "";
            if (component != null)
            {
                string parentname = "";
                Transform parent = component.transform.parent;
                if (parent != null && parent != component.transform)
                    parentname = parent.name + " / ";
                s = parentname + component.name + "." + component.GetType().Name;
            }
            return s;
        }

        public static string GetNameWithParent(UnityEngine.Object obj)
        {
            string s = "";
            if (obj != null)
                s = obj.GetType().Name;
            return s;
        }

        public static object GetPropertyTargetObject(UnityEditor.SerializedProperty property)
        {
            string ppath = property.propertyPath.Replace(".Array.data[", "[");
            object obj = property.serializedObject.targetObject;
            string[] pathElements = ppath.Split('.');
            foreach (string e in pathElements)
            {
                if (e.Contains("["))
                {
                    string elementName = e.Substring(0, e.IndexOf("["));
                    int index = System.Convert.ToInt32(e.Substring(e.IndexOf("[")).Replace("[", "").Replace("]", ""));
                    obj = getValue(obj, elementName, index);
                }
                else
                    obj = getValue(obj, e);
            }
            return obj;
        }

        private static object getValue(object obj, string name)
        {
            if (obj == null)
                return null;

            Type objType = obj.GetType();

            while (objType != null)
            {
                FieldInfo f = objType.GetField(name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                if (f != null)
                    return f.GetValue(obj);

                PropertyInfo p = objType.GetProperty(name, BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.NonPublic | BindingFlags.Public);
                if (p != null)
                    return p.GetValue(obj, null);

                objType = objType.BaseType;
            }
            return null;
        }

        private static object getValue(object obj, string name, int index)
        {
            System.Collections.IEnumerable enumerable = getValue(obj, name) as System.Collections.IEnumerable;
            if (enumerable == null)
                return null;
            System.Collections.IEnumerator enumerator = enumerable.GetEnumerator();

            for (int i = 0; i <= index; i++)
                if (!enumerator.MoveNext())
                    return null;

            return enumerator.Current;
        }

        public class EzEventContainerFoundItem
        {
            public UnityEngine.Object UnityEngineObj;
            public object SystemObj;
            public TimelineAsset TimeLineAsset;
            public PlayableDirector Director;
            public TimelineEzEventsClip EzClip;
            public UnityEngine.Object CEditorclip;
            public TimelineClip CTimeLineClip;

            public EzEventContainerFoundItem(UnityEngine.Object unityEngineObj, object systemObj, TimelineAsset timelineAsset, PlayableDirector director, TimelineEzEventsClip ezClip = null, UnityEngine.Object editorclip = null, TimelineClip timelineClip = null)
            {
                UnityEngineObj = unityEngineObj;
                SystemObj = systemObj;
                TimeLineAsset = timelineAsset;
                Director = director;
                EzClip = ezClip;
                CEditorclip = editorclip;
                CTimeLineClip = timelineClip;
            }
        }

        public static List<EzEventContainerFoundItem> FindAllInstances<T>(object value, Type[] unityTypesFilter, Type[] customTypesFilter) where T : class
        {

            HashSet<object> exploredObjects = new HashSet<object>();
            List<EzEventContainerFoundItem> found = new List<EzEventContainerFoundItem>();
            findAllInstances<T>(value as UnityEngine.Object, null, null, value, exploredObjects, found, unityTypesFilter, customTypesFilter);
            return found;
        }

        private static void findAllInstances<T>(UnityEngine.Object lastBehaviour, TimelineAsset lastTimelineAsset, PlayableDirector lastDirector, object value, HashSet<object> exploredObjects, List<EzEventContainerFoundItem> found, Type[] unityTypesFilter, Type[] customTypesFilter) where T : class
        {
            if (value == null)
                return;

            if (exploredObjects.Contains(value))
                return;

            exploredObjects.Add(value);

            System.Collections.IEnumerable enumerable = value as System.Collections.IEnumerable;

            if (enumerable != null)
            {
                foreach (object item in enumerable)
                    findAllInstances<T>(lastBehaviour, lastTimelineAsset, lastDirector, item, exploredObjects, found, unityTypesFilter, customTypesFilter);
            }
            else
            {
                UnityEngine.Object b = value as UnityEngine.Object;
                if (b != null)
                    lastBehaviour = b;
                TimelineAsset t = value as TimelineAsset;
                if (t != null)
                    lastTimelineAsset = t;
                PlayableDirector d = value as PlayableDirector;
                if (d != null)
                    lastDirector = d;

                T possibleMatch = value as T;
                if (possibleMatch != null)
                {
                    //Debug.LogWarning("FOUND!");
                    found.Add(new EzEventContainerFoundItem(lastBehaviour, possibleMatch, lastTimelineAsset, lastDirector));
                }

                Type type = value.GetType();
                if (unityTypesFilter.AnyMatchWithType(type, false) || (customTypesFilter.AnyMatchWithType(type, true)))
                {
                    //Func<PropertyInfo, bool> predicateProp = x => ((unityTypesFilter.AnyMatchWithType(x.PropertyType, true)) || (customTypesFilter.AnyMatchWithType(x.PropertyType, true)));
                    PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    PropertyInfo[] filteredProperties = properties/*.Where(predicateProp).ToArray()*/;
                    foreach (PropertyInfo prop in filteredProperties)
                    {
                        try
                        {
                            //Debug.LogWarning("prop:" + prop.PropertyType);
                            if (!prop.PropertyType.IsPrimitive)
                            {
                                object propValue = prop.GetValue(value, null);
                                if (propValue != null)
                                    findAllInstances<T>(lastBehaviour, lastTimelineAsset, lastDirector, propValue, exploredObjects, found, unityTypesFilter, customTypesFilter);
                            }
                        }
                        catch (Exception)
                        {
                            //Debug.LogError(e);
                        }
                    }
                    //Func<FieldInfo, bool> predicateField = x => ((unityTypesFilter.AnyMatchWithType(x.FieldType, true)) || (customTypesFilter.AnyMatchWithType(x.FieldType, true)));
                    FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    FieldInfo[] filteredFields = fields/*.Where(predicateField).ToArray()*/;
                    foreach (FieldInfo field in filteredFields)
                    {
                        try
                        {
                            //Debug.LogWarning("field:" + field.FieldType);
                            if (!field.FieldType.IsPrimitive)
                            {
                                object fieldValue = field.GetValue(value);
                                if (fieldValue != null)
                                    findAllInstances<T>(lastBehaviour, lastTimelineAsset, lastDirector, fieldValue, exploredObjects, found, unityTypesFilter, customTypesFilter);
                            }
                        }
                        catch (Exception)
                        {
                            //Debug.LogError(e);
                        }
                    }
                }
            }
        }
#endif
    }

    public static class ExtensionMethods
    {
        public static bool AnyMatchWithType(this Type[] list, Type type, bool inheritance)
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (inheritance)
                {
                    if (type == list[i] || type.IsSubclassOf(list[i]))
                        return true;
                }
                else if (type == list[i])
                    return true;
            }
            return false;
        }

        public static int GetIndexOfType(this Type[] list, List<int> valuesAlreadyUsed, Type type)
        {
            int foundIndex = -1;
            for (int i = 0; i < list.Length; i++)
                if (list[i] == type && valuesAlreadyUsed.IndexOf(i) == -1)
                    return i;
            return foundIndex;
        }

        public static int GetTotalMethods(this List<TimelineEzEventsHelper.BehaviourMethodData> list)
        {
            int totalMethods = 0;
            foreach (TimelineEzEventsHelper.BehaviourMethodData idata in list)
                totalMethods += idata.MethodInfos != null ? idata.MethodInfos.Count : 0;
            return totalMethods;
        }

        public static int GetMethodInfoIndex(this List<TimelineEzEventsHelper.BehaviourMethodData> list, string name)
        {
            int cnt = 0;
            foreach (TimelineEzEventsHelper.BehaviourMethodData idata in list)
            {
                foreach (MethodInfo mi in idata.MethodInfos)
                {
                    if (mi.DeclaringType + "." + mi.Name == name)
                        return cnt;
                    cnt++;
                }
            }
            return -1;
        }

        public static MethodInfo GetMethodInfo(this List<TimelineEzEventsHelper.BehaviourMethodData> list, int index)
        {
            int cnt = 0;
            foreach (TimelineEzEventsHelper.BehaviourMethodData idata in list)
            {
                foreach (MethodInfo mi in idata.MethodInfos)
                {
                    if (cnt == index)
                        return mi;
                    cnt++;
                }
            }
            return null;
        }

        public static List<string> GetAllMethodInfoNames(this List<TimelineEzEventsHelper.BehaviourMethodData> bList, bool addFirstNone)
        {
            List<string> names = new List<string>();
            if (addFirstNone)
                names.Add(" - None - ");
            foreach (TimelineEzEventsHelper.BehaviourMethodData idata in bList)
                foreach (MethodInfo mi in idata.MethodInfos)
                    names.Add(mi.DeclaringType + "." + mi.Name + "(" + mi.GetParamatersAsCSV() + ")");

            return names;
        }

        public static bool MethodHasOverloads(this List<TimelineEzEventsHelper.BehaviourMethodData> bList, string handlerKeyNoArgs)
        {
            foreach (TimelineEzEventsHelper.BehaviourMethodData idata in bList)
                if (idata.MethodInfos.Where(x => (x.DeclaringType + "." + x.Name) == (handlerKeyNoArgs)).Count() > 1)
                    return true;

            return false;
        }

        public static string GetParamatersAsCSV(this MethodInfo mi)
        {
            string s = "";
            if (mi != null)
            {
                ParameterInfo[] parameters = mi.GetParameters();
                for (int i = 0; i < parameters.Length; i++)
                {
                    if (i > 0)
                        s += ",";
                    s += GetAsString(parameters[i]);
                }
            }
            return s;
        }

        public static string GetAsString(this ParameterInfo pi)
        {
            return pi.ParameterType.IsEnum ? pi.ParameterType.FullName : pi.ParameterType.Name;
        }

        public static T[] RemoveAt<T>(this T[] source, int index)
        {
            T[] dest = new T[source.Length - 1];
            if (index > 0)
                Array.Copy(source, 0, dest, 0, index);

            if (index < source.Length - 1)
                Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

            return dest;
        }

        public static T[] SwapElement<T>(this T[] array, int oldIndex, int newIndex)
        {
            T arrElementBak = array[newIndex];
            array[newIndex] = array[oldIndex];
            array[oldIndex] = arrElementBak;
            return array;
        }

        public static T ToType<T>(this string input)
        {
            Type type = typeof(T);
            if (type == typeof(string))
                return (T)(object)input;
            else if (!string.IsNullOrEmpty(input))
            {
                if (type == typeof(float))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        return (T)(object)float.Parse(input);
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(double))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        return (T)(object)double.Parse(input);
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(uint))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        return (T)(object)uint.Parse(input);
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(int))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        return (T)(object)int.Parse(input);
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(Int64))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        return (T)(object)Int64.Parse(input);
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(bool))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        return (T)(object)bool.Parse(input);
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(Vector2))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        if (!string.IsNullOrEmpty(input))
                        {
                            string[] sArr = input.Split(',');
                            if (sArr.Length > 1)
                                return (T)(object)new Vector2(sArr[0].ToType<float>(), sArr[1].ToType<float>());
                        }
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(Vector3))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        if (!string.IsNullOrEmpty(input))
                        {
                            string[] sArr = input.Split(',');
                            if (sArr.Length > 2)
                                return (T)(object)new Vector3(sArr[0].ToType<float>(), sArr[1].ToType<float>(), sArr[2].ToType<float>());
                        }
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(Vector4))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        if (!string.IsNullOrEmpty(input))
                        {
                            string[] sArr = input.Split(',');
                            if (sArr.Length > 3)
                                return (T)(object)new Vector4(sArr[0].ToType<float>(), sArr[1].ToType<float>(), sArr[2].ToType<float>(), sArr[3].ToType<float>());
                        }
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
                else if (type == typeof(Color))
                {
#if UNITY_EDITOR
                    try
                    {
#endif
                        if (!string.IsNullOrEmpty(input))
                        {
                            string[] sArr = input.Split(',');
                            if (sArr.Length > 3)
                                return (T)(object)new Color(sArr[0].ToType<float>(), sArr[1].ToType<float>(), sArr[2].ToType<float>(), sArr[3].ToType<float>());
                        }
#if UNITY_EDITOR
                    }
                    catch (Exception e)
                    {
                        Debug.LogWarning("TimelineEzEvents: " + e);
                    }
#endif
                }
            }
            return default(T);
        }

        public static Type GetEzType(this string input)
        {
            Type type = null;
            switch (input)
            {
                case "EzEvent":
                    type = typeof(EzEvent);
                    break;
                case "FrameData":
                    type = typeof(FrameData);
                    break;
                case "Object":
                    type = typeof(UnityEngine.Object);
                    break;
                case "String":
                    type = typeof(string);
                    break;
                case "Single":
                    type = typeof(float);
                    break;
                case "Double":
                    type = typeof(double);
                    break;
                case "UInt32":
                    type = typeof(uint);
                    break;
                case "Int32":
                    type = typeof(int);
                    break;
                case "Int64":
                    type = typeof(Int64);
                    break;
                case "Boolean":
                    type = typeof(bool);
                    break;
                case "Vector2":
                    type = typeof(Vector2);
                    break;
                case "Vector3":
                    type = typeof(Vector3);
                    break;
                case "Vector4":
                    type = typeof(Vector4);
                    break;
                case "Color":
                    type = typeof(Color);
                    break;
                default:
                    type = TimelineEzEventsHelper.FindType(input);
                    break;
            }
            return type;
        }

        public static string GetAsSerializedString(this Color c)
        {
            return c.r + " , " + c.g + " , " + c.b + " , " + c.a;
        }

        public static string GetAsSerializedString(this Vector4 v)
        {
            return v.x + " , " + v.y + " , " + v.z + " , " + v.w;
        }

        public static string GetAsSerializedString(this Vector3 v)
        {
            return v.x + " , " + v.y + " , " + v.z;
        }

        public static string GetAsSerializedString(this Vector2 v)
        {
            return v.x + " , " + v.y;
        }
    }
}