using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Bizzini.TimelineEzEvents
{
    [Serializable]
    public class TimelineEzEventsClip : PlayableAsset, ITimelineClipAsset
    {
        [HideInInspector] public string UniqueId;
        public TimelineEzEventsBehaviour behaviourTemplate = new TimelineEzEventsBehaviour();
        [NonSerialized] public PlayableDirector LinkedPlayableDirector;
        [NonSerialized] public TimelineClip LinkedTimelineClip;
        [HideInInspector] public TimelineEzEventsTrack LinkedEzTrack;
        [NonSerialized] public int IndexOnTrack;

        public ClipCaps clipCaps
        {
            get { return ClipCaps.None | ClipCaps.Extrapolation; }
        }

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            LinkedPlayableDirector = graph.GetResolver() as PlayableDirector;
            ScriptPlayable<TimelineEzEventsBehaviour> playable = ScriptPlayable<TimelineEzEventsBehaviour>.Create(graph, behaviourTemplate);
            behaviourTemplate = playable.GetBehaviour();
            behaviourTemplate.LinkedClip = LinkedTimelineClip;
            behaviourTemplate.LinkedEzClip = this;
            behaviourTemplate.EventsContainer.LinkedPlayableDirector = LinkedPlayableDirector;
            behaviourTemplate.EventsContainer.TargetGameObject = LinkedEzTrack.GenericBindingGameObject;
            for (int eventIndex = 0; eventIndex < behaviourTemplate.EventsContainer.Events.Length; eventIndex++)
                behaviourTemplate.EventsContainer.Events[eventIndex].Init(behaviourTemplate.EventsContainer, LinkedPlayableDirector, eventIndex, LinkedEzTrack, this);

            return playable;
        }

        public void AssignUniqueId()
        {
            UniqueId = Guid.NewGuid().ToString();
        }

#if UNITY_EDITOR
        public void ClearTimelineClipExposedReferences()
        {
            if (behaviourTemplate.EventsContainer.Events != null)
                for (int eventIndex = 0; eventIndex < behaviourTemplate.EventsContainer.Events.Length; eventIndex++)
                    behaviourTemplate.EventsContainer.Events[eventIndex].ObjectExposedReferenceManager.Clear(LinkedPlayableDirector);
        }

        private void OnDestroy()
        {
            ClearTimelineClipExposedReferences();
        }
#endif
    }
}