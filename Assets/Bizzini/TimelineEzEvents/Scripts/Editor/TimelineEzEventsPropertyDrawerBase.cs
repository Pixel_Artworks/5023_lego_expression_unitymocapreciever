using Bizzini.TimelineEzEvents.Debugging;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;

namespace Bizzini.TimelineEzEvents.EditorStuff
{
    public class TimelineEzEventsPropertyDrawerBase : PropertyDrawer
    {
        protected TimelineEzEventsGuiDrawHelper.DrawManagement drawManager;
        public const float Height_InfoBox = 44f;
        public const float Height_TitleAndButtons = 44f;
        private StringBuilder methodsDropDownSB = new StringBuilder();
        private int ezEventDragSourceIndex = -1;
        private int ezEventDragDestinationIndex = -1;
        private Vector2 ezEventDragMouseStartPos;
        private Vector2 ezEventDragMouseCurOffset;

        public TimelineEzEventsPropertyDrawerBase()
        {
            drawManager = new TimelineEzEventsGuiDrawHelper.DrawManagement(6);
        }

        protected void drawEvents(SerializedProperty containerProperty, Rect boxBgRect, string title, TimelineEzEventsContainer eventsContainer, TimelineEzEventsContainer.OperatingOnEnum operateMode, PlayableDirector director, ref EzEvent[] events, GameObject targetGameObject, TimelineEzEventsClip clip)
        {
            Vector2 mousePosOnEventsRect = Event.current.mousePosition;
            //mousePosOnEventsRect.x -= boxBgRect.x;
            mousePosOnEventsRect.y -= boxBgRect.y;

            boxBgRect.y += drawManager.CurHeight;
            boxBgRect.height -= drawManager.CurHeight;

            drawManager.DrawEditor_Box(boxBgRect, TimelineEzEventsGuiDrawHelper.Color_BackGroundNormal);
            drawTitleAddEventGui(title, operateMode, director, containerProperty, ref events, targetGameObject, clip);
            SerializedProperty eventsArrayProperty = containerProperty.FindPropertyRelative("Events");
            for (int i = 0; i < eventsArrayProperty.arraySize; i++)
            {
                if (ezEventDragDestinationIndex > -1 && i == 0 && ezEventDragDestinationIndex == 0)
                    dragDrawDestinationLine();
                drawEvent(operateMode, eventsContainer, i, ref events, targetGameObject, eventsArrayProperty, clip, director);
                if (ezEventDragDestinationIndex > -1)
                {
                    if (ezEventDragSourceIndex >= ezEventDragDestinationIndex)
                    {
                        if (i == ezEventDragDestinationIndex - 1)
                            dragDrawDestinationLine();
                    }
                    else if (ezEventDragSourceIndex < ezEventDragDestinationIndex)
                    {
                        if (i == ezEventDragDestinationIndex)
                            dragDrawDestinationLine();
                    }
                }
            }

            if (Event.current.type == EventType.MouseDrag || Event.current.type == EventType.DragUpdated || Event.current.type == EventType.DragExited)
            {
                switch (Event.current.type)
                {
                    case EventType.MouseDrag:
                        int mouseIsOverEventIndex = -1;
                        ezEventDragMouseCurOffset = Vector2.zero;
                        if (events.Length > 1)
                        {
                            for (int i = 0; i < events.Length; i++)
                            {
                                if (events[i].GuiBounds.Contains(mousePosOnEventsRect))
                                {
                                    mouseIsOverEventIndex = i;
                                    events[i].GuiIsDragging = true;
                                }
                                else
                                    events[i].GuiIsDragging = false;
                            }

                            if (mouseIsOverEventIndex > -1)
                            {
                                ezEventDragMouseStartPos = mousePosOnEventsRect;
                                ezEventDragSourceIndex = mouseIsOverEventIndex;
                                DragAndDrop.PrepareStartDrag();
                                DragAndDrop.objectReferences = null;
                                DragAndDrop.StartDrag("");
                            }
                        }
                        break;
                    case EventType.DragUpdated:
                        if (ezEventDragSourceIndex > -1)
                        {
                            ezEventDragMouseCurOffset = mousePosOnEventsRect - ezEventDragMouseStartPos;
                            ezEventDragMouseCurOffset.x = 0;
                            ezEventDragDestinationIndex = ezEventDragSourceIndex;
                            Rect sourceEventBounds = events[ezEventDragSourceIndex].GuiBounds;
                            for (int i = 0; i < events.Length; i++)
                            {
                                if (i != ezEventDragSourceIndex)
                                {
                                    if ((sourceEventBounds.center.y >= events[i].GuiBounds.y || i == 0) && (sourceEventBounds.center.y < events[i].GuiBounds.yMax || i == events.Length - 1))
                                    {
                                        if (sourceEventBounds.center.y <= events[i].GuiBounds.center.y)
                                            ezEventDragDestinationIndex = i - 1 < 0 ? 0 : i - 1;
                                        else
                                            ezEventDragDestinationIndex = i;
                                    }
                                }
                            }
                        }
                        break;
                    case EventType.DragExited:
                        if (ezEventDragSourceIndex > -1)
                        {
                            if (ezEventDragSourceIndex != ezEventDragDestinationIndex && ezEventDragDestinationIndex > -1)
                                swapEvent(containerProperty, targetGameObject, clip, events, director, ezEventDragSourceIndex, ezEventDragDestinationIndex);
                            ezEventDragSourceIndex = -1;
                            ezEventDragDestinationIndex = -1;
                            for (int i = 0; i < events.Length; i++)
                                events[i].GuiIsDragging = false;
                        }
                        break;
                }
                if (ezEventDragSourceIndex > -1)
                    Event.current.Use();
            }
        }

        protected TimelineEzEventsContainerDrawer.DrawInfoBoxCheckResult drawInfoBoxAndCheckAssignment(Rect boxBgRect, PlayableDirector director, GameObject targetGameObject, TimelineEzEventsContainer.OperatingOnEnum operateMode)
        {
            boxBgRect.y += drawManager.CurHeight;
            boxBgRect.height = Height_InfoBox;

            float oldHeight = drawManager.CurHeight;
            drawManager.DrawEditor_Box(boxBgRect, TimelineEzEventsGuiDrawHelper.Color_InfoBackGroundNormal);

            TimelineEzEventsContainerDrawer.DrawInfoBoxCheckResult result = new TimelineEzEventsContainerDrawer.DrawInfoBoxCheckResult();

            EditorGUI.BeginChangeCheck();

            result.TargetGameObject = targetGameObject;
            EditorGUI.BeginDisabledGroup(true);
            result.Director = drawManager.DrawEditor_ObjectField("Director", "Playable director that is controlling the timeline. Remove this if you want to raise event manually", 0, director, typeof(PlayableDirector), true) as PlayableDirector;
            EditorGUI.EndDisabledGroup();

            if (operateMode == TimelineEzEventsContainer.OperatingOnEnum.TimelineClip)
            {
                EditorGUI.BeginDisabledGroup(true);
                drawManager.DrawEditor_ObjectField("Invoke target (Track)", "The GameObject referenced by Track that contains methods to invoke", 0, targetGameObject, typeof(UnityEngine.Object), true);
                EditorGUI.EndDisabledGroup();
            }
            else
                result.TargetGameObject = drawManager.DrawEditor_ObjectField("Invoke target", "The GameObject that contains methods to invoke", 0, targetGameObject, typeof(UnityEngine.Object), true); //when editing clip you cannot assign TargetGameObject manually

            if (!Application.isPlaying && operateMode != TimelineEzEventsContainer.OperatingOnEnum.TimelineClip && EditorGUI.EndChangeCheck())
                if (targetGameObject != null)
                    UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);

            drawManager.CurHeight = oldHeight;
            drawManager.AddHeight(TimelineEzEventsGuiDrawHelper.DefaultLineHeight * 0.5f + Height_InfoBox);
            return result;
        }

        protected bool drawTitleAddEventGui(string titleName, TimelineEzEventsContainer.OperatingOnEnum operateMode, PlayableDirector director, SerializedProperty containerProperty, ref EzEvent[] events, GameObject targetGameObject, TimelineEzEventsClip clip = null)
        {
            if (operateMode == TimelineEzEventsContainer.OperatingOnEnum.TimelineClip)
                drawManager.BeginFreezeHeight();
            drawManager.DrawEditor_LabelField(titleName, "", 0, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_Title);
            if (operateMode == TimelineEzEventsContainer.OperatingOnEnum.TimelineClip)
            {
                drawManager.StopFreezeHeight();
                SerializedProperty clipTimeSnapProperty = containerProperty.FindPropertyRelative("ClipTimeSnap");
                clipTimeSnapProperty.boolValue = drawManager.DrawEditor_ToggleCol1("Time Snap", "Snap Timeline time\nto Clip start   ", clipTimeSnapProperty.boolValue, 84, -100, 1f);
            }

            SerializedProperty eventsArrayProperty = containerProperty.FindPropertyRelative("Events");
            if (drawManager.DrawEditor_Button("+", "Create new event", 0, TimelineEzEventsGuiDrawHelper.Color_BtnEventsTools, 30, 0.9f, 0f))
            {
                Undo.RecordObject(containerProperty.serializedObject.targetObject, "(TimelineEzEvent)Added new Event");   //UNDO MAYBE BUGGED
                events = TimelineEzEventsHelper.AddEvent(events);
                events[events.Length - 1] = clip != null ? new EzEvent(clip) : new EzEvent();
                eventsArrayProperty.serializedObject.Update();
                if (!Application.isPlaying && clip == null)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
                eventsArrayProperty.serializedObject.ApplyModifiedProperties();
                return true;
            }
            return false;
        }

        protected int drawEvent(TimelineEzEventsContainer.OperatingOnEnum operateMode, TimelineEzEventsContainer eventsContainer, int eventIndex, ref EzEvent[] events, GameObject trackTargetGameObject, SerializedProperty eventsContainerProperty, TimelineEzEventsClip clip, PlayableDirector director)
        {
            if (operateMode != TimelineEzEventsContainer.OperatingOnEnum.Manual && director == null)
                return eventIndex;

            SerializedProperty singleEventProperty = eventsContainerProperty.GetArrayElementAtIndex(eventIndex);
            SerializedProperty isEnabledProperty = singleEventProperty.FindPropertyRelative("IsEnabled");
            SerializedProperty evaluateInEditModeProperty = singleEventProperty.FindPropertyRelative("EvaluateInEditMode");
            SerializedProperty methodConfigsArr = singleEventProperty.FindPropertyRelative("MethodConfigurations");
            SerializedProperty conditionResultInvertedProperty = singleEventProperty.FindPropertyRelative("ConditionResultInverted");
            SerializedProperty showFirstComponentOnlyMethodsProperty = singleEventProperty.FindPropertyRelative("ShowFirstComponentOnly");
            SerializedProperty showStaticMethodsProperty = singleEventProperty.FindPropertyRelative("ShowStaticMethods");
            SerializedProperty showBaseMethodsProperty = singleEventProperty.FindPropertyRelative("ShowBaseMethods");
            SerializedProperty showPropertiesProperty = singleEventProperty.FindPropertyRelative("ShowProperties");
            SerializedProperty showVoidOnlyReturnTypeProperty = singleEventProperty.FindPropertyRelative("ShowVoidOnlyReturnType");
            SerializedProperty onClipInvocationTimeProperty = singleEventProperty.FindPropertyRelative("OnClipInvocationTime");
            SerializedProperty onTimelineInvocationTimeProperty = singleEventProperty.FindPropertyRelative("OnTimelineInvocationTime");

            drawManager.CurRectGlobalOffset = events[eventIndex].GuiIsDragging ? ezEventDragMouseCurOffset : Vector2.zero;

            events[eventIndex].GuiBounds = drawManager.CurRect;
            events[eventIndex].GuiBounds.x += drawManager.CurRectGlobalOffset.x;
            events[eventIndex].GuiBounds.y = drawManager.CurHeight + drawManager.CurRectGlobalOffset.y;

            EditorGUI.BeginDisabledGroup(events[eventIndex].GuiIsDragging);
            drawManager.DrawEditor_LabelField("Event: " + eventIndex, "", 0, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_EventName);
            isEnabledProperty.boolValue = drawManager.DrawEditor_Toggle("Enabled", "", isEnabledProperty.boolValue);
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(!isEnabledProperty.boolValue || events[eventIndex].GuiIsDragging);

            evaluateInEditModeProperty.boolValue = drawManager.DrawEditor_Toggle("Evaluate in EditMode", "", evaluateInEditModeProperty.boolValue);
            switch (operateMode)
            {
                case TimelineEzEventsContainer.OperatingOnEnum.TimelineClip:
                    drawManager.DrawEditor_Enum(typeof(EzEvent.OnClipInvocationTimeEnum), onClipInvocationTimeProperty, null, "Invocation mode");
                    break;
                case TimelineEzEventsContainer.OperatingOnEnum.Manual:
                    EditorGUI.BeginDisabledGroup(true);
                    drawManager.DrawEditor_Enum(typeof(EzEvent.OnFreeInvocationTimeEnum), onTimelineInvocationTimeProperty, null, "Invocation mode");
                    EditorGUI.EndDisabledGroup();
                    break;
            }

            drawManager.BeginFreezeHeight();
            drawManager.DrawEditor_LabelField("Methods filter", "", 0);
            Rect r;
            showFirstComponentOnlyMethodsProperty.boolValue = drawManager.DrawEditor_Toggle("", "", showFirstComponentOnlyMethodsProperty.boolValue, 16);
            r = drawManager.GetPos(drawManager.GetColumn_Horiz_Two(drawManager.CurRect, 1), drawManager.CurHeight, 14);
            r.width = 20f;
            EditorGUI.LabelField(r, new GUIContent("Fc", "Retrieve methods from first component only"));

            showStaticMethodsProperty.boolValue = drawManager.DrawEditor_Toggle("", "", showStaticMethodsProperty.boolValue, 16, 34);
            r = drawManager.GetPos(drawManager.GetColumn_Horiz_Two(drawManager.CurRect, 1), drawManager.CurHeight, 48);
            r.width = 20f;
            EditorGUI.LabelField(r, new GUIContent("St", "Static methods"));

            showBaseMethodsProperty.boolValue = drawManager.DrawEditor_Toggle("", "", showBaseMethodsProperty.boolValue, 16, 68);
            r = drawManager.GetPos(drawManager.GetColumn_Horiz_Two(drawManager.CurRect, 1), drawManager.CurHeight, 82);
            r.width = 20f;
            EditorGUI.LabelField(r, new GUIContent("In", "Inherited methods"));

            r = drawManager.GetPos(drawManager.GetColumn_Horiz_Two(drawManager.CurRect, 1), drawManager.CurHeight, 116);
            r.width = 20f;
            EditorGUI.LabelField(r, new GUIContent("Pr", "Properties (Get;Set;)"));
            showPropertiesProperty.boolValue = drawManager.DrawEditor_Toggle("", "", showPropertiesProperty.boolValue, 16, 102);

            r = drawManager.GetPos(drawManager.GetColumn_Horiz_Two(drawManager.CurRect, 1), drawManager.CurHeight, 150);
            r.width = 20f;
            EditorGUI.LabelField(r, new GUIContent("Vo", "Void only return type (Conditions not affected)"));
            drawManager.StopFreezeHeight();
            showVoidOnlyReturnTypeProperty.boolValue = drawManager.DrawEditor_Toggle("", "", showVoidOnlyReturnTypeProperty.boolValue, 16, 136);

            drawManager.BeginFreezeHeight();
            if (drawManager.DrawEditor_Button("", conditionResultInvertedProperty.boolValue ? "!" : "", "Invert condition result", null, 14f, 0.84f, 0f, 0f))
                conditionResultInvertedProperty.boolValue = !conditionResultInvertedProperty.boolValue;
            drawManager.StopFreezeHeight();

            Type[][] methodReplacedOldArgsTypes = new Type[3][];    //REF is needed for this when passing it to functions

            MethodInfo selectedConditionMethod = drawMethod("Condition", "", 16f, 0, operateMode, methodConfigsArr, ref methodReplacedOldArgsTypes[0], director, clip, eventIndex, eventsContainer, events, trackTargetGameObject, showFirstComponentOnlyMethodsProperty.boolValue, showStaticMethodsProperty.boolValue, showBaseMethodsProperty.boolValue, showPropertiesProperty.boolValue, typeof(bool));
            if (selectedConditionMethod == null)
                events[eventIndex].ObjectExposedReferenceManager.Clear(director, 0);

            MethodInfo selectedMethod = drawMethod(selectedConditionMethod != null ? "On True" : "Method", "    ", 0, 1, operateMode, methodConfigsArr, ref methodReplacedOldArgsTypes[1], director, clip, eventIndex, eventsContainer, events, trackTargetGameObject, showFirstComponentOnlyMethodsProperty.boolValue, showStaticMethodsProperty.boolValue, showBaseMethodsProperty.boolValue, showPropertiesProperty.boolValue, (showVoidOnlyReturnTypeProperty.boolValue ? typeof(void) : null));
            if (selectedMethod == null)
                events[eventIndex].ObjectExposedReferenceManager.Clear(director, 1);

            if (selectedConditionMethod != null)
                drawMethod("On False", "    ", 0, 2, operateMode, methodConfigsArr, ref methodReplacedOldArgsTypes[2], director, clip, eventIndex, eventsContainer, events, trackTargetGameObject, showFirstComponentOnlyMethodsProperty.boolValue, showStaticMethodsProperty.boolValue, showBaseMethodsProperty.boolValue, showPropertiesProperty.boolValue, (showVoidOnlyReturnTypeProperty.boolValue ? typeof(void) : null));
            else
                events[eventIndex].ObjectExposedReferenceManager.Clear(director, 2);

            EditorGUI.EndDisabledGroup();
            drawManager.BeginFreezeHeight();

            //if (eventIndex > 0)
            //{
            //    if (drawManager.DrawEditor_Button("", "^", "Move event up", TimelineEzEventsGuiDrawHelper.Color_BtnEventsTools, eventIndex >= events.Length - 1 ? 62 : 30, 0.8f, 0f))
            //        swapEvent(eventsContainerProperty, trackTargetGameObject, clip, events, director, eventIndex, eventIndex - 1);
            //}
            //if (eventIndex < events.Length - 1)
            //{
            //    if (drawManager.DrawEditor_Button("", "v", "Move event down", TimelineEzEventsGuiDrawHelper.Color_BtnEventsTools, eventIndex <= 0 ? 62 : 30, 0.8f, 0f, eventIndex > 0 ? 32f : 0))
            //        swapEvent(eventsContainerProperty, trackTargetGameObject, clip, events, director, eventIndex, eventIndex + 1);
            //}

            EditorGUI.BeginDisabledGroup(events[eventIndex].GuiIsDragging);
            if (drawManager.DrawEditor_Button("C", "Copy", 1, TimelineEzEventsGuiDrawHelper.Color_BtnEventsTools, 22, 0.8f, 1f, -44))
                events[eventIndex].ToClipboard();

            if (drawManager.DrawEditor_Button("P", "Paste", 1, TimelineEzEventsGuiDrawHelper.Color_BtnEventsTools, 22, 0.8f, 1f, -20))
                events[eventIndex].FromClipboard();

            drawManager.StopFreezeHeight();

            if (drawManager.DrawEditor_Button("X", "Delete this event", 1, TimelineEzEventsGuiDrawHelper.Color_BtnEventsDelete, 18, 0.8f, 1f))
            {
                Undo.RecordObjects(new UnityEngine.Object[] { eventsContainerProperty.serializedObject.targetObject }, "(TimelineEzEvent)Deleted Event");   //UNDO BUGGED
                events = TimelineEzEventsHelper.DeleteEvent(events, director, eventIndex);
                eventIndex--;
                eventsContainerProperty.serializedObject.Update();
                if (!Application.isPlaying && clip == null)
                    if (trackTargetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trackTargetGameObject.scene);
                eventsContainerProperty.serializedObject.ApplyModifiedProperties();
                return eventIndex;
            }
            EditorGUI.EndDisabledGroup();
            events[eventIndex].GuiBounds.height = drawManager.CurHeight + drawManager.CurRectGlobalOffset.y - events[eventIndex].GuiBounds.y;
            drawManager.CurRectGlobalOffset = Vector2.zero;
            return eventIndex;
        }

        private void dragDrawDestinationLine()
        {
            Rect dragDestRect = drawManager.GetPos(drawManager.GetColumn_Horiz_Two(drawManager.CurRect, 0), drawManager.CurHeight - 4);
            dragDestRect.width *= 0.8f;
            dragDestRect.height = 2f;
            drawManager.DrawEditor_Rect(dragDestRect, new Color(0.3f, 0.4f, 1f));
        }

        private void swapEvent(SerializedProperty eventsContainerProperty, GameObject trackTargetGameObject, TimelineEzEventsClip clip, EzEvent[] events, PlayableDirector director, int sourceIndex, int destIndex)
        {
            TimelineEzEventsHelper.SwapEvent(events, director, sourceIndex, destIndex);
            eventsContainerProperty.MoveArrayElement(sourceIndex, destIndex);
            if (!Application.isPlaying && clip == null)
                if (trackTargetGameObject != null)
                    UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trackTargetGameObject.scene);
        }

        private MethodInfo drawMethod(string title, string labelprefix, float horizontalOffset, int methodIndex, TimelineEzEventsContainer.OperatingOnEnum operateMode, SerializedProperty methodConfigsArrProp, ref Type[] methodReplacedOldArgsTypes, PlayableDirector director, TimelineEzEventsClip clip, int eventIndex, TimelineEzEventsContainer eventsContainer, EzEvent[] events, GameObject trackTargetGameObject, bool showFirstComponentOnly, bool showStaticMethods, bool showBaseMethods, bool showProperties, Type returnType = null)
        {
            if (methodIndex >= methodConfigsArrProp.arraySize)
                return null;

            MethodInfo method = drawMethodsDropDown(labelprefix + title, horizontalOffset, -1, operateMode, director, eventsContainer, events[eventIndex], eventIndex, methodConfigsArrProp, methodIndex, ref methodReplacedOldArgsTypes, trackTargetGameObject, returnType, false, showFirstComponentOnly, showStaticMethods, showBaseMethods, showProperties, clip);
            if (method != null)
            {
                ParameterInfo[] methodCurrentArgsParameters = method.GetParameters();

                SerializedProperty argStringValuesArrCurrentProperty = methodConfigsArrProp.GetArrayElementAtIndex(methodIndex).FindPropertyRelative("ArgValues");
                SerializedProperty argTypesStringValuesArrCurrentProperty = methodConfigsArrProp.GetArrayElementAtIndex(methodIndex).FindPropertyRelative("ArgTypes");

                int prevSize = argStringValuesArrCurrentProperty.arraySize;
                if (argStringValuesArrCurrentProperty.arraySize < methodCurrentArgsParameters.Length)
                    argStringValuesArrCurrentProperty.arraySize = methodCurrentArgsParameters.Length;
                for (int i = prevSize; i < argStringValuesArrCurrentProperty.arraySize; i++)
                    argStringValuesArrCurrentProperty.GetArrayElementAtIndex(i).stringValue = "";
                argTypesStringValuesArrCurrentProperty.arraySize = argStringValuesArrCurrentProperty.arraySize;

                List<string> newArgsMappedToOld = null;
                if (methodReplacedOldArgsTypes != null && methodReplacedOldArgsTypes.Length > 0)
                    newArgsMappedToOld = createArgsOrderedWithOldMethod(methodCurrentArgsParameters, methodReplacedOldArgsTypes, argStringValuesArrCurrentProperty);

                int argObjCnt = 0;
                for (int i = 0; i < methodCurrentArgsParameters.Length; i++)
                {
                    ParameterInfo methodParamInfo = methodCurrentArgsParameters[i];
                    argTypesStringValuesArrCurrentProperty.GetArrayElementAtIndex(i).stringValue = methodParamInfo.GetAsString();
                    SerializedProperty argValueProperty = argStringValuesArrCurrentProperty.GetArrayElementAtIndex(i);
                    string stringValue = (methodReplacedOldArgsTypes != null && methodReplacedOldArgsTypes.Length > 0) ? newArgsMappedToOld[i] : argStringValuesArrCurrentProperty.GetArrayElementAtIndex(i).stringValue;
                    drawMethodArg(labelprefix, operateMode, method, methodIndex, argValueProperty, stringValue, methodParamInfo, argObjCnt, events[eventIndex], clip, trackTargetGameObject, director, eventIndex);
                    if (methodParamInfo.ParameterType == typeof(UnityEngine.Object))
                        argObjCnt++;
                }

                if (argStringValuesArrCurrentProperty.arraySize > methodCurrentArgsParameters.Length)
                    argStringValuesArrCurrentProperty.arraySize = methodCurrentArgsParameters.Length;
            }
            return method;
        }

        protected MethodInfo drawMethodsDropDown(string label, float horizontalOffset, float width, TimelineEzEventsContainer.OperatingOnEnum operateMode, PlayableDirector director, TimelineEzEventsContainer eventsContainer, EzEvent ezEvent, int eventIndex, SerializedProperty methodArrConfigProperty, int methodIndex, ref Type[] methodReplacedOldArgsTypes, GameObject gameObject, Type returnType, bool noArguments, bool showFirstComponentOnly, bool showStaticMethods, bool showBaseMethods, bool showProperties, TimelineEzEventsClip clip)
        {
            if (gameObject == null)
                return null;

            SerializedProperty methodNameProp = methodArrConfigProperty.GetArrayElementAtIndex(methodIndex).FindPropertyRelative("HandlerKey");

            List<TimelineEzEventsHelper.BehaviourMethodData> suitableBehaviourInvocationData = TimelineEzEventsHelper.GetSuitableInvocationData(gameObject, returnType, noArguments, showFirstComponentOnly, showStaticMethods, showBaseMethods, showProperties);
            string[] dropDownMethodNames = suitableBehaviourInvocationData.GetAllMethodInfoNames(true).ToArray();
            GUIContent[] dropDownMethodDisplayNames = new GUIContent[dropDownMethodNames.Length];

            int oldIndex = 0;
            if (!string.IsNullOrEmpty(methodNameProp.stringValue))
            {
                int foundMethodIndex = TimelineEzEventsHelper.IndexOfMethodName(dropDownMethodNames, methodNameProp.stringValue);
                TimelineEzEventsHelper.MethodNameArgsStruct methodNameStruct = new TimelineEzEventsHelper.MethodNameArgsStruct(methodNameProp.stringValue);
                if (foundMethodIndex == -1)
                {
                    if (TimelineEzEventsConfig.LossMethodProtection)
                    {
                        string similarMethodName = TimelineEzEventsHelper.MethodHandlerKeyNameSimilarityLostLookup(suitableBehaviourInvocationData, methodNameStruct);
                        if (TimelineEzEventsMethodLossDialog.Prompt(methodNameProp.stringValue, similarMethodName))
                        {
                            Debug.LogWarning("TimelineEzEvents - Method before replace: " + ezEvent.MethodConfigurations[methodIndex].CreateStringSnapshot(ezEvent, methodIndex));
                            foundMethodIndex = TimelineEzEventsHelper.IndexOfMethodName(dropDownMethodNames, similarMethodName);
                            methodReplacedOldArgsTypes = new Type[methodNameStruct.TargetMethodArgTypes.Length];
                            Array.Copy(methodNameStruct.TargetMethodArgTypes, methodReplacedOldArgsTypes, methodNameStruct.TargetMethodArgTypes.Length);
                        }
                        else
                        {
                            Selection.activeObject = null;
                            return null;
                        }
                    }
                    else
                        TimelineEzEventsDebugger.ConsoleLogLostMethod(false, gameObject, methodNameProp.stringValue);
                }
                oldIndex = Mathf.Clamp(foundMethodIndex, 0, int.MaxValue);
            }

            for (int i = 0; i < dropDownMethodNames.Length; i++)
            {
                dropDownMethodDisplayNames[i] = new GUIContent(dropDownMethodNames[i], dropDownMethodNames[i]);
                if (i > 0)
                {
                    methodsDropDownSB.Length = 0;
                    int openParenthesisPos = dropDownMethodDisplayNames[i].text.IndexOf('(');
                    string fullMethod = dropDownMethodDisplayNames[i].text.Substring(0, openParenthesisPos);
                    int lastDotPos = fullMethod.LastIndexOf('.');
                    if (lastDotPos < 0)
                        lastDotPos = 0;
                    string method = fullMethod.Substring(lastDotPos + 1, fullMethod.Length - lastDotPos - 1);
                    string methodPath = fullMethod.Substring(0, fullMethod.Length - method.Length - 1);
                    string arguments = dropDownMethodDisplayNames[i].text.Substring(fullMethod.Length,
                        dropDownMethodDisplayNames[i].text.Length - fullMethod.Length);
                    methodsDropDownSB.Append(methodPath.Replace('.', '/'));
                    methodsDropDownSB.Append("/");

                    if (TimelineEzEventsConfig.GroupMethodsByUnderscore)
                    {
                        int underscorePos = method.IndexOf('_');
                        if (underscorePos > -1 && underscorePos < method.Length - 1)
                        {
                            char[] chars = method.ToCharArray();
                            chars[underscorePos] = '/';
                            method = new string(chars);
                        }
                    }

                    methodsDropDownSB.Append(method);
                    methodsDropDownSB.Append(arguments);
                    dropDownMethodDisplayNames[i].text = methodsDropDownSB.Length < 196 ? methodsDropDownSB.ToString() : methodsDropDownSB.ToString().Substring(0, 196) + "...";
                }
            }

            int newIndex = drawManager.DrawEditor_Dropdown(label, "", oldIndex, null, dropDownMethodDisplayNames, horizontalOffset, width, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodTitle);
            methodNameProp.stringValue = (newIndex == 0 ? null : dropDownMethodNames[newIndex]);

            if (!Application.isPlaying && clip == null && oldIndex != newIndex)
                if (gameObject != null)
                    UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(gameObject.scene);
            return suitableBehaviourInvocationData.GetMethodInfo(newIndex - 1);
        }

        private List<string> createArgsOrderedWithOldMethod(ParameterInfo[] methodCurrentArgsParameters, Type[] methodReplacedOldArgsTypes, SerializedProperty argValuesCurrentStringArrayProperty)
        {
            List<int> usedValues = new List<int>(methodCurrentArgsParameters.Length);
            List<string> newArgValues = new List<string>(methodCurrentArgsParameters.Length);
            for (int i = 0; i < methodCurrentArgsParameters.Length; i++)
            {
                int index = i;
                string value = "";
                int foundTypePos = methodReplacedOldArgsTypes.GetIndexOfType(usedValues, methodCurrentArgsParameters[i].ParameterType);
                if (foundTypePos > -1 && foundTypePos < argValuesCurrentStringArrayProperty.arraySize)
                {
                    index = foundTypePos;
                    usedValues.Add(foundTypePos);
                    value = argValuesCurrentStringArrayProperty.GetArrayElementAtIndex(index).stringValue;
                }
                newArgValues.Add(value);
            }
            return newArgValues;
        }

        protected void drawMethodArg(string labelprefix, TimelineEzEventsContainer.OperatingOnEnum operateMode, MethodInfo selectedMethod, int methodIndex, SerializedProperty argValueProperty, string stringValue, ParameterInfo param, int argObjIndex, EzEvent evt, TimelineEzEventsClip clip, GameObject targetGameObject, PlayableDirector director, int eventIndex)
        {
            Type type = param.ParameterType;
            string paramName = labelprefix + " " + param.Name;

            if (type.IsEnum)
                drawManager.DrawEditor_Enum(param.ParameterType, argValueProperty, stringValue, paramName, -1, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
            else if (type == typeof(bool))
            {
                bool oldValue = stringValue.ToType<bool>();
                bool newBoolValue = drawManager.DrawEditor_Toggle(paramName, type.ToString(), oldValue, 16f, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newBoolValue.ToString();
                if (!Application.isPlaying && clip == null && oldValue != newBoolValue)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(Vector2))
            {
                Vector2 oldVector2 = stringValue.ToType<Vector2>();
                Vector2 newVector2 = drawManager.DrawEditor_Vector2Field(paramName, type.ToString(), oldVector2, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newVector2.GetAsSerializedString();
                if (!Application.isPlaying && clip == null && oldVector2 != newVector2)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(Vector3))
            {
                Vector3 oldVector3 = stringValue.ToType<Vector3>();
                Vector3 newVector3 = drawManager.DrawEditor_Vector3Field(paramName, type.ToString(), oldVector3, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newVector3.GetAsSerializedString();
                if (!Application.isPlaying && clip == null && oldVector3 != newVector3)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(Vector4))
            {
                Vector4 oldVector4 = stringValue.ToType<Vector4>();
                Vector4 newVector4 = drawManager.DrawEditor_Vector4Field(paramName, type.ToString(), oldVector4, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newVector4.GetAsSerializedString();
                if (!Application.isPlaying && clip == null && oldVector4 != newVector4)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(Color))
            {
                Color oldColor = stringValue.ToType<Color>();
                Color newColor = drawManager.DrawEditor_ColorField(paramName, type.ToString(), oldColor, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newColor.GetAsSerializedString();
                if (!Application.isPlaying && clip == null && oldColor != newColor)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(UnityEngine.Object))
            {
                EzEvent.ObjectExposedReferenceManagement.ObjectExposedInfo objExposedInfo = evt.ObjectExposedReferenceManager.Get(methodIndex, argObjIndex);
                UnityEngine.Object oldObj = objExposedInfo != null ? objExposedInfo.ResolvedObj : null;
                UnityEngine.Object newObj = drawManager.DrawEditor_ObjectField(paramName, type.ToString(), 0, oldObj, typeof(UnityEngine.Object), true, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);

                string exposedName = TimelineEzEventsHelper.GenerateObjectReferenceExposedName(clip, eventIndex, methodIndex, argObjIndex);
                evt.ObjectExposedReferenceManager.Set(exposedName, argObjIndex, eventIndex, methodIndex, argObjIndex, newObj);
                if (!Application.isPlaying && (oldObj != newObj || (director != null && objExposedInfo != null && !objExposedInfo.ObjIsStored(director, exposedName))))
                {
                    evt.ObjectExposedReferenceManager.Store(methodIndex, argObjIndex, director, false);
                    switch (operateMode)
                    {
                        case TimelineEzEventsContainer.OperatingOnEnum.TimelineClip:
                            if (director != null)
                            {
                                UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(director.gameObject.scene);
                                EditorUtility.SetDirty(director.gameObject);
                            }
                            break;
                        case TimelineEzEventsContainer.OperatingOnEnum.Manual:
                            if (targetGameObject != null)
                            {
                                UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
                                EditorUtility.SetDirty(targetGameObject);
                            }
                            break;
                    }
                }
            }
            else if (type == typeof(FrameData))
            {
                drawManager.DrawEditor_LabelField("* " + paramName, type.ToString(), 0, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
            }
            else if (type == typeof(EzEvent))
            {
                drawManager.DrawEditor_LabelField("* " + paramName, type.ToString(), 0, 0, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
            }
            else if (type == typeof(float))
            {
                float oldFloat = stringValue.ToType<float>();
                float newFloat = drawManager.DrawEditor_FloatField(paramName, type.ToString(), oldFloat, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newFloat.ToString();
                if (!Application.isPlaying && clip == null && oldFloat != newFloat)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(double))
            {
                double oldDouble = stringValue.ToType<double>();
                double newDouble = drawManager.DrawEditor_DoubleField(paramName, type.ToString(), oldDouble, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newDouble.ToString();
                if (!Application.isPlaying && clip == null && oldDouble != newDouble)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(int))
            {
                int oldInt = stringValue.ToType<int>();
                int newInt = drawManager.DrawEditor_IntField(paramName, type.ToString(), oldInt, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newInt.ToString();
                if (!Application.isPlaying && clip == null && oldInt != newInt)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(uint))
            {
                uint oldInt = stringValue.ToType<uint>();
                uint newInt = (uint)drawManager.DrawEditor_DoubleField(paramName, type.ToString(), (double)Mathf.Clamp(oldInt, 0, uint.MaxValue), TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newInt.ToString();
                if (!Application.isPlaying && clip == null && oldInt != newInt)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(Int64))
            {
                Int64 oldInt = stringValue.ToType<Int64>();
                Int64 newInt = (Int64)drawManager.DrawEditor_DoubleField(paramName, type.ToString(), (double)Mathf.Clamp(oldInt, 0, Int64.MaxValue), TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newInt.ToString();
                if (!Application.isPlaying && clip == null && oldInt != newInt)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            else if (type == typeof(string))
            {
                string oldString = stringValue;
                string newString = drawManager.DrawEditor_TextField(paramName, type.ToString(), oldString, TimelineEzEventsGuiDrawHelper.StyleLabel_MethodArgs);
                argValueProperty.stringValue = newString;
                if (!Application.isPlaying && clip == null && oldString != newString)
                    if (targetGameObject != null)
                        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(targetGameObject.scene);
            }
            argValueProperty.serializedObject.ApplyModifiedProperties();
        }

        protected virtual float calculateEventsHeight(bool eventsDataCheckOk, EzEvent[] events)
        {
            float totalHeight = 0;
            totalHeight += Height_InfoBox;
            if (eventsDataCheckOk)
            {
                totalHeight += (TimelineEzEventsGuiDrawHelper.DefaultLineHeight * 0.5f);
                totalHeight += Height_TitleAndButtons;
                if (events != null)
                {
                    for (int eventIndex = 0; eventIndex < events.Length; eventIndex++)
                    {
                        EzEvent evt = events[eventIndex];
                        bool hasConditionMethod = !string.IsNullOrEmpty(evt.MethodConfigurations[0].HandlerKey);
                        totalHeight += TimelineEzEventsGuiDrawHelper.DefaultLineHeight * (hasConditionMethod ? 9f : 8f);
                        if (evt.MethodConfigurations != null)
                        {
                            for (int i = 0; i < evt.MethodConfigurations.Length; i++)
                            {
                                if (i != 2 || hasConditionMethod)
                                    totalHeight += evt.MethodConfigurations[i].ArgValues == null || evt.MethodConfigurations[i].ArgValues.Length <= 0 || string.IsNullOrEmpty(evt.MethodConfigurations[i].HandlerKey) ? 0 : TimelineEzEventsGuiDrawHelper.DefaultLineHeight * evt.MethodConfigurations[i].ArgValues.Length;
                            }
                        }
                    }
                }
            }
            return totalHeight;
        }
    }
}