using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;

namespace Bizzini.TimelineEzEvents.EditorStuff
{
    [CustomPropertyDrawer(typeof(TimelineEzEventsContainer), true)]
    public class TimelineEzEventsContainerDrawer : TimelineEzEventsPropertyDrawerBase
    {
        private bool eventsDataCheckOk;
        private PlayableDirector director;
        private TimelineEzEventsClip clip;
        private GameObject targetGameObject;
        private TimelineEzEventsContainer eventsContainer;
        private string folderName;

        public override bool CanCacheInspectorGUI(SerializedProperty containerProperty)
        {
            return false;
        }

        public override void OnGUI(Rect windowRect, SerializedProperty containerProperty, GUIContent label)
        {
            processDataAndRender(windowRect, containerProperty, label, false);
            return;
        }

        private void processDataAndRender(Rect windowRect, SerializedProperty containerProperty, GUIContent label, bool dontRender)
        {
            int oldIndent = EditorGUI.indentLevel;
            eventsDataCheckOk = true;
            director = null;
            clip = null;
            targetGameObject = null;
            eventsContainer = null;

            if (containerProperty.serializedObject.targetObject is TimelineEzEventsClip)
            {
                folderName = "Events";
                clip = containerProperty.serializedObject.targetObject as TimelineEzEventsClip;
                eventsContainer = clip.behaviourTemplate.EventsContainer;
                eventsContainer.OperateMode = TimelineEzEventsContainer.OperatingOnEnum.TimelineClip;
                targetGameObject = clip.LinkedEzTrack != null ? clip.LinkedEzTrack.GenericBindingGameObject : null;
                director = clip.LinkedPlayableDirector;
            }
            else
            {
                folderName = containerProperty.propertyPath.Substring(containerProperty.propertyPath.LastIndexOf('.') + 1);
                eventsContainer = TimelineEzEventsHelper.GetPropertyTargetObject(containerProperty) as TimelineEzEventsContainer;
                if (eventsContainer == null)
                    return;
                eventsContainer.OperateMode = TimelineEzEventsContainer.OperatingOnEnum.None;
                if (eventsContainer.Events == null)
                    eventsContainer.Events = new EzEvent[0];
                targetGameObject = eventsContainer.TargetGameObject;
                director = eventsContainer.LinkedPlayableDirector;
            }

            Rect windowRectIndented = EditorGUI.IndentedRect(windowRect);
            if (!dontRender)
            {
                containerProperty.isExpanded = EditorGUI.Foldout(new Rect(windowRect.x, windowRect.y, windowRect.width, TimelineEzEventsGuiDrawHelper.DefaultLineHeight), containerProperty.isExpanded, folderName);
                EditorGUI.indentLevel = 0;
            }
            if (containerProperty.isExpanded || dontRender)
            {
                DrawInfoBoxCheckResult drawInfoBoxCheckResult;
                if (!dontRender)
                {
                    drawManager.Begin(windowRectIndented);
                    drawManager.AddLine();
                    drawInfoBoxCheckResult = drawInfoBoxAndCheckAssignment(windowRectIndented, director, targetGameObject, eventsContainer.OperateMode);
                    director = drawInfoBoxCheckResult.Director;
                    eventsContainer.TargetGameObject = drawInfoBoxCheckResult.TargetGameObject as GameObject;
                }
                else
                {
                    drawInfoBoxCheckResult = new DrawInfoBoxCheckResult();
                    drawInfoBoxCheckResult.TargetGameObject = targetGameObject;
                    drawInfoBoxCheckResult.Director = director;
                }

                if (eventsContainer.OperateMode == TimelineEzEventsContainer.OperatingOnEnum.None)
                    eventsContainer.OperateMode = TimelineEzEventsContainer.OperatingOnEnum.Manual;

                string eventsTitle = "Events";

                if (eventsContainer.OperateMode == TimelineEzEventsContainer.OperatingOnEnum.TimelineClip)
                {
                    eventsTitle = "RAISED by TIMELINE CLIP";
                    clip.LinkedPlayableDirector = director;
                    if ((clip == null) || !drawInfoBoxCheckResult.IsOk())
                    {
                        eventsDataCheckOk = false;
                        EditorUtility.SetDirty(containerProperty.serializedObject.targetObject);
                        return;
                    }
                }
                else if (eventsContainer.OperateMode == TimelineEzEventsContainer.OperatingOnEnum.Manual)
                {
                    eventsTitle = "RAISED MANUALLY";
                    eventsContainer.LinkedPlayableDirector = director;
                    if (drawInfoBoxCheckResult.TargetGameObject == null)
                    {
                        eventsDataCheckOk = false;
                        EditorUtility.SetDirty(containerProperty.serializedObject.targetObject);
                        return;
                    }
                }

                if (!dontRender && !containerProperty.serializedObject.isEditingMultipleObjects)
                    drawEvents(containerProperty, windowRectIndented, eventsTitle, eventsContainer, eventsContainer.OperateMode, director, ref eventsContainer.Events, targetGameObject, clip);
            }
            if (!dontRender)
                EditorGUI.indentLevel = oldIndent;
        }

        public struct DrawInfoBoxCheckResult
        {
            public PlayableDirector Director;
            public UnityEngine.Object TargetGameObject;

            public bool IsOk()
            {
                return Director != null && TargetGameObject != null;
            }
        }

        public override float GetPropertyHeight(SerializedProperty containerProperty, GUIContent label)
        {
            eventsContainer = TimelineEzEventsHelper.GetPropertyTargetObject(containerProperty) as TimelineEzEventsContainer;
            processDataAndRender(Rect.zero, containerProperty, label, true);
            if (eventsContainer != null)
                return TimelineEzEventsGuiDrawHelper.DefaultLineHeight + (containerProperty.isExpanded ? calculateEventsHeight(eventsDataCheckOk && !containerProperty.serializedObject.isEditingMultipleObjects, eventsContainer.Events) : 0);
            return 0;
        }
    }
}