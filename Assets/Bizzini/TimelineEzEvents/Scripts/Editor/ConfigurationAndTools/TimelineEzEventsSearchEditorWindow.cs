namespace Bizzini.TimelineEzEvents.EditorStuff
{
    using UnityEngine;
    using UnityEditor;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Reflection;
    using UnityEngine.Timeline;
    using UnityEngine.Playables;

    public class TimelineEzEventsSearchEditorWindow : EditorWindow
    {
        private static TimelineEzEventsSearchEditorWindow curWindow;
        private List<TimelineEzEventsHelper.EzEventContainerFoundItem> foundList = new List<TimelineEzEventsHelper.EzEventContainerFoundItem>();
        private Vector2 searchTypesScrollPos;
        private Vector2 searchResultsScrollPos;
        private const int searchTypesHeight = 80;
        private string searchInTypesText;
        private string searchMethodName;
        private string lastMsg;
        private GUIStyle styleSearchTypesTextArea;
        private GUIStyle styleSearchToggleResults;
        private bool searchInvalidMethodsOnly;

        [MenuItem("Tools/Bizzini/TimelineEzEvents/Search")]
        static void Init()
        {
            curWindow = (TimelineEzEventsSearchEditorWindow)GetWindow(typeof(TimelineEzEventsSearchEditorWindow));
            getConfigData();
            curWindow.foundList = new List<TimelineEzEventsHelper.EzEventContainerFoundItem>();
            curWindow.Show();
        }

        private static void getConfigData()
        {
            curWindow.searchInvalidMethodsOnly = TimelineEzEventsConfig.SearchInvalidMethodsOnly;
            curWindow.searchInTypesText = TimelineEzEventsConfig.SearchTypes;
            curWindow.searchMethodName = TimelineEzEventsConfig.SearchMethodName;
        }

        void OnGUI()
        {
            if (styleSearchTypesTextArea == null)
            {
                styleSearchTypesTextArea = new GUIStyle(EditorStyles.textField);
                styleSearchTypesTextArea.wordWrap = true;
                styleSearchToggleResults = new GUIStyle(EditorStyles.label);
                styleSearchToggleResults.margin = styleSearchToggleResults.border = new RectOffset(0, 250, 0, 0);
            }

            EditorGUILayout.LabelField("Custom Types where EzEvents are stored  (eg: NameSpace.MyClass+MyNestedClass)");
            searchTypesScrollPos = EditorGUILayout.BeginScrollView(searchTypesScrollPos, GUILayout.Height(searchTypesHeight));

            searchInTypesText = EditorGUILayout.TextArea(searchInTypesText, styleSearchTypesTextArea, GUILayout.MaxHeight(searchTypesHeight));
            EditorGUILayout.EndScrollView();

            EditorGUI.BeginDisabledGroup(searchInvalidMethodsOnly);
            EditorGUILayout.LabelField("Filter method name (including path)");
            searchMethodName = EditorGUILayout.TextField(searchMethodName);
            EditorGUI.EndDisabledGroup();
            searchInvalidMethodsOnly = EditorGUILayout.ToggleLeft("Filter invalid/lost methods only", searchInvalidMethodsOnly);

            if (GUILayout.Button("Search EzEvents Containers"))
            {
                foundList = new List<TimelineEzEventsHelper.EzEventContainerFoundItem>();
                List<Type> unityTypesFilter = new List<Type>()
                {
                typeof(PlayableAsset),
                typeof(UnityEngine.Object),
                typeof(List<TimelineClip>),
                typeof(List<TrackAsset>),
                typeof(Behaviour),
                typeof(PlayableDirector),
                typeof(TimelineAsset),
                typeof(TimelineClip),
                typeof(TimelineEzEventsContainer),
                typeof(TimelineEzEventsTrack),
                typeof(TimelineEzEventsClip),
                typeof(TimelineEzEventsBehaviour),
                };
                List<Type> customTypesFilter = new List<Type>();

                lastMsg = "";
                if (!string.IsNullOrEmpty(searchInTypesText))
                {
                    string[] typesAsStrings = searchInTypesText.Split('\n');
                    if (typesAsStrings.Length > 0)
                    {
                        List<Type> additionalTypes = new List<Type>(typesAsStrings.Length);
                        foreach (string typeAsString in typesAsStrings)
                        {
                            if (!string.IsNullOrEmpty(typeAsString))
                            {
                                string typeClean = typeAsString.Trim();
                                Type type = TimelineEzEventsHelper.FindType(typeClean);
                                if (type != null)
                                    additionalTypes.Add(type);
                                else
                                    lastMsg += (lastMsg.Length > 0 ? "\n" : "") + "Cannot find type: " + typeClean;
                            }
                        }
                        customTypesFilter.AddRange(additionalTypes);
                    }
                }
                bool searchByName = !string.IsNullOrEmpty(searchMethodName);
                for (int sceneIndex = 0; sceneIndex < UnityEngine.SceneManagement.SceneManager.sceneCount; sceneIndex++)
                {
                    UnityEngine.SceneManagement.Scene scene = UnityEngine.SceneManagement.SceneManager.GetSceneAt(sceneIndex);
                    foreach (GameObject go in scene.GetRootGameObjects())
                    {
                        Behaviour[] behaviours = go.GetComponentsInChildren<Behaviour>(true);
                        for (int goIndex = 0; goIndex < behaviours.Length; goIndex++)
                        {
                            Behaviour c = behaviours[goIndex];
                            List<TimelineEzEventsHelper.EzEventContainerFoundItem> foundEzEventsContainers = TimelineEzEventsHelper.FindAllInstances<TimelineEzEventsContainer>(c, unityTypesFilter.ToArray(), customTypesFilter.ToArray());

                            foreach (TimelineEzEventsHelper.EzEventContainerFoundItem item in foundEzEventsContainers)
                            {
                                prepareFoundItem(item);
                                if (item.SystemObj != null)
                                {
                                    TimelineEzEventsContainer ezEventContainer = item.SystemObj as TimelineEzEventsContainer;
                                    if (ezEventContainer.Events != null && ezEventContainer.Events.Length > 0)
                                    {
                                        bool foundLostMethod = false;
                                        bool foundMethodName = false;
                                        foreach (EzEvent ezEvt in ezEventContainer.Events)
                                        {
                                            for (int i = 0; i < ezEvt.MethodConfigurations.Length; i++)
                                            {
                                                if (i == 2 && string.IsNullOrEmpty(ezEvt.MethodConfigurations[0].HandlerKey))   //there's no condition method thus don't look inside False method
                                                    continue;

                                                EzEvent.MethodConfiguration mconf = ezEvt.MethodConfigurations[i];

                                                GameObject boundGameObject = item.Director == null ? (item.UnityEngineObj as Component).gameObject : (GameObject)item.Director.GetGenericBinding(item.CTimeLineClip.parentTrack);
                                                if (boundGameObject != null)
                                                {
                                                    if (!string.IsNullOrEmpty(mconf.HandlerKey))
                                                    {
                                                        if (searchInvalidMethodsOnly)
                                                        {
                                                            List<TimelineEzEventsHelper.BehaviourMethodData> suitableBehaviourInvocationData = TimelineEzEventsHelper.GetSuitableInvocationData(boundGameObject, (i == 0 ? typeof(bool) : ezEvt.ShowVoidOnlyReturnType ? typeof(void) : null), false, ezEvt.ShowFirstComponentOnly, ezEvt.ShowStaticMethods, ezEvt.ShowBaseMethods, ezEvt.ShowProperties);
                                                            List<string> methodNames = suitableBehaviourInvocationData.GetAllMethodInfoNames(false);
                                                            if (!methodNames.Contains(mconf.HandlerKey))
                                                            {
                                                                foundLostMethod = true;
                                                                break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (searchByName)
                                                            {
                                                                if (!foundMethodName)
                                                                {
                                                                    foundMethodName = TimelineEzEventsHelper.MethodNameArgsStruct.GetMethodNameNoArgsFromFull(mconf.HandlerKey).Contains(searchMethodName);
                                                                    if (foundMethodName)
                                                                        break;  //stop searching methods inside this event
                                                                }
                                                            }
                                                            else
                                                                break;  //stop searching methods inside this event
                                                        }
                                                    }
                                                }
                                            }
                                            if (searchInvalidMethodsOnly)
                                            {
                                                if (foundLostMethod)
                                                    break;  //stop searching events inside this container
                                            }
                                            else
                                            {
                                                if (searchByName)
                                                {
                                                    if (foundMethodName)
                                                        break;  //stop searching events inside this container
                                                }
                                            }
                                        }

                                        if (searchInvalidMethodsOnly)
                                        {
                                            if (foundLostMethod)
                                                addToFoundList(item);
                                        }
                                        else
                                        {
                                            if (searchByName)
                                            {
                                                if (foundMethodName)
                                                    addToFoundList(item);
                                            }
                                            else
                                                addToFoundList(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                TimelineEzEventsConfig.SearchInvalidMethodsOnly = searchInvalidMethodsOnly;
                TimelineEzEventsConfig.SearchTypes = searchInTypesText;
                TimelineEzEventsConfig.SearchMethodName = searchMethodName;
                TimelineEzEventsConfig.Save();
            }
            if (!string.IsNullOrEmpty(lastMsg))
                EditorGUILayout.HelpBox(lastMsg, MessageType.Warning, true);
            drawSearchResults();
        }

        private void addToFoundList(TimelineEzEventsHelper.EzEventContainerFoundItem item)
        {
            Predicate<TimelineEzEventsHelper.EzEventContainerFoundItem> match = x => x.SystemObj == item.SystemObj;
            if (foundList != null && foundList.Find(match) == null)
                foundList.Add(item);
        }

        private void prepareFoundItem(TimelineEzEventsHelper.EzEventContainerFoundItem item)
        {
            item.EzClip = item.UnityEngineObj as TimelineEzEventsClip;

            if (item.EzClip != null)
            {
                item.CTimeLineClip = TimelineEzEventsHelper.GetTimelineClip(item.TimeLineAsset, item.EzClip);
                Type typeEditorClip = Type.GetType("UnityEditor.Timeline.EditorClip, UnityEditor.Timeline");
                item.CEditorclip = ScriptableObject.CreateInstance(typeEditorClip);
                PropertyInfo propEditorClip = typeEditorClip.GetProperty("clip");
                propEditorClip.SetValue(item.CEditorclip, item.CTimeLineClip, null);
            }
        }

        private void drawSearchResults()
        {
            searchResultsScrollPos = EditorGUILayout.BeginScrollView(searchResultsScrollPos);
            for (int i = 0; i < foundList.Count; i++)
            {
                TimelineEzEventsHelper.EzEventContainerFoundItem item = foundList[i];
                drawFoundItem(i, item);
            }

            EditorGUILayout.EndScrollView();
        }

        private void drawFoundItem(int index, TimelineEzEventsHelper.EzEventContainerFoundItem item)
        {
            string objName = item.EzClip != null ? "(TIMELINE) " + TimelineEzEventsHelper.GetNameWithParent(item.Director) : "(CUSTOM) " + TimelineEzEventsHelper.GetNameWithParent(item.UnityEngineObj as Component);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(index.ToString("D5"), GUILayout.MaxWidth(45));
            if (EditorGUILayout.ToggleLeft(objName, false, styleSearchToggleResults, null))
            {
                if (item.EzClip != null)
                {
                    Type typeTimelineWindow = TimelineEzEventsHelper.FindType("UnityEditor.Timeline.TimelineWindow");
                    //EditorWindow editorWindow = EditorWindow.GetWindow(typeTimelineWindow);
                    //editorWindow.Show();
                    PropertyInfo propTimelineWindowsInstance = typeTimelineWindow.GetProperty("instance");
                    object timelineEditorWindow = propTimelineWindowsInstance.GetValue(null, null);
                    MethodInfo mi = typeTimelineWindow.GetMethod("SetCurrentTimeline", new Type[] { typeof(PlayableDirector), typeof(TimelineClip) });
                    mi.Invoke(timelineEditorWindow, new object[] { item.Director, null });
                    Selection.SetActiveObjectWithContext(item.CEditorclip, item.Director);
                }
                else
                    Selection.activeObject = item.UnityEngineObj;
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}
